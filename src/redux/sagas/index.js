import { all } from "redux-saga/effects";
import dataActions from "./dataActions";
import fetchActions from "./fetchActions";

//poner todas nuestras sagas
export default function* rootSaga() {
  yield all([dataActions(), fetchActions()]);
}
