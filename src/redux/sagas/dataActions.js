import { put, call, takeLatest, takeEvery } from "redux-saga/effects";
import {
  requestDog,
  requestDogSuccess,
  requestDogError
} from "../actions/dataActions";
// import { GET_CATEGORIAS } from "../actions/dataActions";
// //put: poner las acciones (dispatch)
// //call: llamada a la API
// //takeLatest: funcion generadora escuchando, takeLatest(ACTION_NAME, getCategorias)

// export function* getCategorias({ payload }) {
//   try {
//     console.log("la funcion esta escuchando");
//   } catch (err) {}
// }

// //funcion para exportar todas las funciones de mis dataActions
// // Watcher
// export default function* dataActions() {
//   yield takeLatest(GET_CATEGORIAS, getCategorias);
// }

// Sagas
export default function* watchFetchDog() {
  yield takeEvery("FETCHED_DOG", fetchDogAsync);
}

export function* fetchDogAsync() {
  try {
    yield put(requestDog());
    const data = yield call(() => {
      return fetch("https://dog.ceo/api/breeds/image/random").then(res =>
        res.json()
      );
    });
    yield put(requestDogSuccess(data));
  } catch (error) {
    yield put(requestDogError());
  }
}
