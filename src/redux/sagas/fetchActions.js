import { put, call, takeLatest, takeEvery } from "redux-saga/effects";
import axios from "axios";
import {
  requestCategorias,
  requestCategoriasSuccess,
  requestCategoriasFailed,
  requestHome,
  requestHomeSuccess,
  requestHomeFailed
} from "../actions/fetchActions";

export function fetchApi() {
  return axios.get("https://www.sanborns.com.mx/producto/getSucursales/");
}

export function fetchApi2() {
  return axios.get("http://clauluna.com/CLARO/api/getHomeSears");
}

// Sagas
export default function* watchFetchCategorias() {
  yield takeEvery("FETCHED_CATEGORIAS", fetchCategorias);
  yield takeEvery("FETCHED_HOME", fetchCategorias);
}

export function* fetchCategorias() {
  try {
    yield put(requestCategorias());
    //llamamos a la función
    const data = yield call(fetchApi);
    //pasamos la data a requestCategorias
    yield put(requestCategoriasSuccess(data.data.sucursales));
  } catch (error) {
    yield put(requestCategoriasFailed());
  }
}

export function* fetchHome() {
  try {
    yield put(requestHome());
    //llamamos a la función
    const data = yield call(fetchApi2);
    //pasamos la data a requestHome
    yield put(requestHomeSuccess(data.data));
  } catch (error) {
    yield put(requestHomeFailed());
  }
}
