// import { SET_CATEGORIAS } from "../types";

// const initialState = {
//   categorias: []
// };

// export default function(state = initialState, action) {
//   switch (action.type) {
//     case SET_CATEGORIAS:
//       return {
//         ...state,
//         categorias: action.payload
//       };
//     default:
//       return state;
//   }
// }

// Reducer
const initialState = {
  url: "",
  loading: false,
  error: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case "REQUESTED_DOG":
      return {
        url: "",
        loading: true,
        error: false
      };
    case "REQUESTED_DOG_SUCCEEDED":
      return {
        url: action.url,
        loading: false,
        error: false
      };
    case "REQUESTED_DOG_FAILED":
      return {
        url: "",
        loading: false,
        error: true
      };
    default:
      return state;
  }
}
