const initialState = {
  datos: [{ id: 1 }, { id: 2 }],
  loading: false,
  error: false,
  datos2: [{ id: 1 }, { id: 2 }],
  loading2: false,
  error2: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case "REQUESTED_CATEGORIAS":
      return {
        datos: [],
        loading: true,
        error: false
      };
    case "REQUESTED_CATEGORIAS_SUCCEEDED":
      return {
        datos: action.datos,
        loading: false,
        error: false
      };
    case "REQUESTED_CATEGORIAS_FAILED":
      return {
        datos: [],
        loading: false,
        error: true
      };
    case "REQUESTED_HOME":
      return {
        datos2: [],
        loading2: true,
        error2: false
      };
    case "REQUESTED_HOME_SUCCEEDED":
      return {
        datos2: action.datos,
        loading2: false,
        error2: false
      };
    case "REQUESTED_HOME_FAILED":
      return {
        datos2: [],
        loading2: false,
        error2: true
      };
    default:
      return state;
  }
}
