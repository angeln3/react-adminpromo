import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
// //sagas
import createSagaMiddleware from "redux-saga";
import rootSagas from "./sagas/index";
//reducers
import dataReducers from "./reducers/dataReducers";
import fetchReducers from "./reducers/fetchReducers";
// import rootSaga from "./sagas";
// //reducers
// import dataReducers from "./reducers/dataReducers";

// const initialState = {};

// const middleware = [thunk];

//si tenemos mas reducers los agregamos
const reducers = combineReducers({
  dogs: dataReducers,
  catego: fetchReducers
});

// // const store = createStore(
// //   reducers,
// //   initialState,
// //   compose(
// //     applyMiddleware(...middleware),
// //     window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
// //   )
// // );

// // export default store;

// const configureStore = () => {
//   const sagaMiddleware = createSagaMiddleware();
//   const store = createStore(
//     reducers,
//     initialState,
//     compose(
//       applyMiddleware(sagaMiddleware),
//       window.__REDUX_DEVTOOLS_EXTENSION__ &&
//         window.__REDUX_DEVTOOLS_EXTENSION__()
//     )
//   );
//   sagaMiddleware.run(rootSaga);
//   return store;
// };

// export default configureStore;

// Store
const sagaMiddleware = createSagaMiddleware();
const store = createStore(
  reducers,
  compose(
    applyMiddleware(sagaMiddleware),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )
);
sagaMiddleware.run(rootSagas);

export default store;
