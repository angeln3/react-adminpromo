// import { SET_CATEGORIAS } from "../types";
// import axios from "axios";

// export const GET_CATEGORIAS = "GET_CATEGORIAS";

// //si viene info agragamos por parametro los datos
// export const getCategorias = () => dispatch => {
//   axios
//     .get("https://www.sanborns.com.mx/producto/getSucursales/")
//     .then(res => {
//       dispatch({
//         type: SET_CATEGORIAS,
//         payload: res.data
//       });
//       //dispatch(otraAccion());
//     })
//     .catch(err => console.log(err));
// };

// export const getCategorias1 = payload => ({
//   type: GET_CATEGORIAS,
//   ...payload
// });

// Action Creators
export const requestDog = () => {
  return { type: "REQUESTED_DOG" };
};

export const requestDogSuccess = data => {
  return { type: "REQUESTED_DOG_SUCCEEDED", url: data.message };
};

export const requestDogError = () => {
  return { type: "REQUESTED_DOG_FAILED" };
};

export const fetchDog = () => {
  return { type: "FETCHED_DOG" };
};
