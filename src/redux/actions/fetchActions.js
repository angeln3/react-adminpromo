// Action Creators
export const requestCategorias = () => {
  return { type: "REQUESTED_CATEGORIAS" };
};

export const requestCategoriasSuccess = data => {
  return { type: "REQUESTED_CATEGORIAS_SUCCEEDED", datos: data };
};

export const requestCategoriasFailed = () => {
  return { type: "REQUESTED_CATEGORIAS_FAILED" };
};

export const fetchCategorias = () => {
  return { type: "FETCHED_CATEGORIAS" };
};

//home
export const requestHome = () => {
  return { type: "REQUESTED_HOME" };
};

export const requestHomeSuccess = data => {
  return { type: "REQUESTED_HOME_SUCCEEDED", datos: data };
};

export const requestHomeFailed = () => {
  return { type: "REQUESTED_HOME_FAILED" };
};

export const fetchHome = () => {
  return { type: "FETCHED_HOME" };
};
