import React, { Component } from "react";
import Fragment from "render-fragment";
//Switchery
import Switch from "react-switchery-component";
import { Link } from "react-router-dom";
//modal
import EliminarTarjeta from "../utils/modals/EliminarTarjeta";

export class VerBines extends Component {
  constructor(props) {
    super(props);
    this.state = { isChecked: false };
  }

  handleChange = e => {
    const { name, checked } = e.target;
    console.log(name, checked);
    this.setState({ isChecked: checked });
  };
  render() {
    const { isChecked } = this.state;
    return (
      <Fragment>
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Ver Bines </h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item">
                  <a href="#">Home</a>
                </li>
                <li class="breadcrumb-item">
                  <a href="#">Administrador de bines</a>
                </li>
                <li class="breadcrumb-item">
                  <a href="#">Buen Fin</a>
                </li>
                <li class="breadcrumb-item active">
                  <a href="#">Tarjetas</a>
                </li>
              </ol>
            </nav>

            <div class="row">
              <div class="col-md-12 col-sm-12  ">
                <div
                  class="x_panel"
                  style={{ boxShadow: "1px 4px 23px -13px black" }}
                >
                  <div class="x_title">
                    <h2>Lista de Bines</h2>

                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">
                    <div
                      class="x_panel"
                      style={{ boxShadow: "1px 4px 23px -13px black" }}
                    >
                      <h2 class="col-md-12 breadcrumb">
                        Lista de Bines asociados a Afirme
                      </h2>

                      <div class="clearfix"></div>

                      <div class="table-responsive">
                        <table class="table table-striped">
                          <thead>
                            <tr class="headings">
                              <th class="column-title">Nombre </th>
                              <th class="column-title">Tipo </th>

                              <th class="column-title">Bin</th>
                              <th class="column-title">Estado</th>
                              <th class="column-title">Acciones</th>
                            </tr>
                          </thead>

                          <tbody>
                            <tr class="even pointer">
                              <td class=" ">Puntos Afirme</td>
                              <td class=" ">Tarjeta de Crédito</td>

                              <td>58452</td>

                              <td class=" ">
                                <Switch
                                  color="#26B99A"
                                  name="58452"
                                  checked={isChecked}
                                  onChange={e => this.handleChange(e)}
                                />
                              </td>
                              <td class=" ">
                                <div class="btn-group">
                                  <button
                                    type="button"
                                    class="btn btn-primary dropdown-toggle"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                  >
                                    Acciones
                                  </button>
                                  <div class="dropdown-menu">
                                    <Link
                                      class="dropdown-item"
                                      to="/EditarTergetaBin"
                                    >
                                      <i class="fa fa-edit"></i>&nbsp; Editar
                                      Tarjeta
                                    </Link>

                                    <a
                                      class="dropdown-item"
                                      data-toggle="modal"
                                      data-target=".modal-eliminar-tarjeta"
                                    >
                                      <i class="fa fa-trash"></i>&nbsp; Eliminar
                                      Tarjeta
                                    </a>
                                  </div>
                                </div>
                              </td>
                            </tr>

                            <tr class="even pointer">
                              <td class=" ">Monedero Afirme</td>

                              <td class=" ">Tarjeta de Crédito</td>

                              <td>58452</td>

                              <td class=" ">
                                <Switch
                                  color="#26B99A"
                                  name="58452"
                                  checked={isChecked}
                                  onChange={e => this.handleChange(e)}
                                />
                              </td>
                              <td class=" ">
                                <div class="btn-group">
                                  <button
                                    type="button"
                                    class="btn btn-primary dropdown-toggle"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                  >
                                    Acciones
                                  </button>
                                  <div class="dropdown-menu">
                                    <Link
                                      class="dropdown-item"
                                      to="/EditarTergetaBin"
                                    >
                                      <i class="fa fa-edit"></i>&nbsp; Editar
                                      Tarjeta
                                    </Link>

                                    <a
                                      class="dropdown-item"
                                      data-toggle="modal"
                                      data-target=".modal-eliminar-tarjeta"
                                    >
                                      <i class="fa fa-trash"></i>&nbsp; Eliminar
                                      Tarjeta
                                    </a>
                                  </div>
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* modal */}
        <EliminarTarjeta />
        {/* /modal */}
      </Fragment>
    );
  }
}

export default VerBines;
