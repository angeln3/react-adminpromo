import React, { Component, Fragment } from "react";
//Switchery
import Switch from "react-switchery";
//Formik
import { Formik, Form, Field, ErrorMessage } from 'formik';
//link
import {Link} from 'react-router-dom';
//validaciones inputs
import {SchemaEditarTarjeta} from '../utils/validations/ProfileSchema';
//jQuery
import $ from 'jquery';
//modals
import LLenarDatos from '../utils/modals/LLenarDatos';
import ActualizarPromo from '../utils/modals/ActualizarPromo';

class EditarTergetaBin extends Component {
  constructor(props) {
    super(props);
    this.state = {
	  NombreTarjeta: "",	
	  TipoTarjeta: "",
	  Bin: "",
      Estatus: true
    };
  }
  ObtenerInfoInput = e => {
    const { name, value } = e.target;

    this.setState({
      [name]: value
    });
  };

  onChangeSwitch = value => {
    this.setState({
      Estatus: value
    });
  };

  onSubmit = (e) =>{
	e.preventDefault();

	const {
		NombrePromocion,
		TipoTarjeta,
		Bin,
		Estatus
	} = this.state;

	if(NombrePromocion == "" ||
	TipoTarjeta == "" ||
	Bin == "" ||
	Bin.length < 4 ||  
	Estatus == ""
	){
		$("#modalLLenarDatos2").click();
	}else{
		var newDatos ={
			NombrePromocion,
			TipoTarjeta,
			Bin,
			Estatus
		}
		$('#modalDatosActualizados2').click();
		console.log('Enviando...', newDatos);
	}

}

  render() {
    const { Estatus } = this.state;
    return (
		<Fragment>
			<div className="right_col" role="main">
        <div className="">
          <div className="page-title">
            <div className="title_left">
              <h3>Editar Tarjeta </h3>
            </div>
          </div>

          <div className="clearfix"></div>

          <nav aria-label="breadcrumb">
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <a href="#">Home</a>
              </li>
              <li className="breadcrumb-item">
                <a href="#">Administrador de bines</a>
              </li>
              <li className="breadcrumb-item">
                <a href="#">Buen Fin</a>
              </li>
              <li className="breadcrumb-item active">
                <a href="#">Tarjetas</a>
              </li>
              <li className="breadcrumb-item active">
                <a href="#">Edit</a>
              </li>
            </ol>
          </nav>

          <div className="row">
            <div className="col-md-12 col-sm-12">
              <div
                className="x_panel"
                style={{ boxShadow: "1px 4px 23px -13px black" }}
              >
                <h2 className="mb-4">Editar Tarjeta Afirme</h2>

                <div className="clearfix"></div>

                <div className="col-md-12 col-sm-12 d-flex justify-content-center">
                  <div
                    className="x_panel col-md-10 mb-5"
                    style={{ boxShadow: "1px 4px 23px -13px black" }}
                  >
                    <h2 className=" col-md-12 col-sm-12 breadcrumb">
                      Datos de la Tarjeta
                    </h2>

                    <div className="clearfix"></div>

                    <div className="x_content">
                      
                    <Formik
													initialValues={{ NombreTarjeta: '', TipoTarjeta: '', Bin: '' }}
													validationSchema={SchemaEditarTarjeta}
													onSubmit={(values, { setSubmitting }) => {
														setTimeout(() => {
															console.log(values);
															alert(JSON.stringify(values, null, 2));
															setSubmitting(false);
														}, 400);
													}}
												>
													{({
														values,
														errors,
														touched,
														handleChange,
														handleBlur,
														handleSubmit,
														isSubmitting,
														isValid
														/* and other goodies */
													}) => (
														<form onSubmit={this.onSubmit}>
															 <p>Nombre del la tarjeta*</p>
																<input
																	type="text"
																	name="NombreTarjeta"
																	className={errors.NombreTarjeta && touched.NombreTarjeta ? "form-control is-invalid transition" : "form-control mb-4"}
																	onBlur={handleBlur}
																	onChange={(event) => {this.ObtenerInfoInput(event); handleChange(event)}}
																	value={values.NombreTarjeta}
																/>
																{errors.NombreTarjeta && touched.NombreTarjeta && 
																	<p className="text-danger mt-0 transition">{errors.NombreTarjeta}</p>
																}

															<p>Tipo de Tarjeta*</p>
															<select
																name="TipoTarjeta"
																className={errors.TipoTarjeta && touched.TipoTarjeta ? "form-control is-invalid transition" : "form-control mb-4"}
																onChange={(event) => {this.ObtenerInfoInput(event); handleChange(event)}}
																onBlur={handleBlur}
																value={values.TipoTarjeta}
																
															>
																<option value="">
																	Seleccione uno
																</option>
                                <option>Tarjeta de crédito</option>
                                <option>Tarjeta de débito</option>
															</select>
															{errors.TipoTarjeta && touched.TipoTarjeta && 
																<p className="text-danger mt-0 transition">{errors.TipoTarjeta}</p>
															}

														
                                <p>Bin*</p>
																<input
																	type="text"
																	name="Bin"
																	className={errors.Bin && touched.Bin ? "form-control is-invalid transition" : "form-control mb-4"}
																	onBlur={handleBlur}
																	onChange={(event) => {this.ObtenerInfoInput(event); handleChange(event)}}
																	value={values.Bin}
																/>
																{errors.Bin && touched.Bin && 
																	<p className="text-danger mt-0 transition">{errors.Bin}</p>
																}


															<div className="col-md-12 col-sm-3 mb-5 pl-0">
																<p className="col-md-1 mr-2">Estado*</p>

																<Switch
																	className="switch-class"
																	onChange={this.onChangeSwitch}
																	options={{
																		color: "rgb(38, 185, 154)",
																		size: "small"
																	}}
																	checked={Estatus}
																/>
															</div>

															<div className="col-md-12 col-sm-3 text-center mb-5">

																<Link className="btn" to="/VerBines">Cancelar</Link>
	
																<button
																	disabled={isSubmitting}
																	className="btn btn-success"
																	data-toggle="modal"
																	data-target=".actualizar-promo"
																	type="submit"
																>
																	Actualizar
																</button>

																<button
																	id="modalLLenarDatos2"
																	hidden
																	data-toggle="modal"
																	data-target=".modal-LLenarDatosEditar2"
																>		
																</button>

																<button
																	id="modalDatosActualizados2"
																	hidden
																	data-toggle="modal"
																	data-target=".actualizar-promo2"
																>		
																</button>
															</div>
														</form>
													)}
												</Formik>   

								</div>
								</div>
							</div>
							</div>
						</div>
						</div>
					</div>
					</div>
					{/* modal */}
					<ActualizarPromo clase="modal fade actualizar-promo2" />
					<LLenarDatos clase="modal fade modal-LLenarDatosEditar2" />
					{/* /modal */}
		</Fragment>
      
    );
  }
}

export default EditarTergetaBin;
