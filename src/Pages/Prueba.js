import React, { Component } from "react";
//Switchery
import Switch from "react-switchery";
import { Link } from "react-router-dom";

//redux stuff
import { connect } from "react-redux";
import { fetchDog } from "../redux/actions/dataActions";
import { fetchCategorias } from "../redux/actions/fetchActions";
import { fetchHome } from "../redux/actions/fetchActions";

export class Prueba extends Component {
  render() {
    const { dog, categorias } = this.props;
    return (
      <div className="right_col" role="main">
        <button onClick={() => this.props.fetchDog()}>Show Dog</button>
        {dog.loading ? (
          <p>Loading...</p>
        ) : dog.error ? (
          <p>Error, try again</p>
        ) : (
          <p>
            <img src={dog.url} />
          </p>
        )}
        <button onClick={() => this.props.fetchCategorias()}>Show Dog</button>
        {categorias.loading ? (
          <p>Loading...</p>
        ) : categorias.error ? (
          <p>Error, try again</p>
        ) : (
          categorias.datos.map(datos => {
            return <p>{datos.id}</p>;
          })
        )}
        <button onClick={() => this.props.fetchHome()}>Show catego</button>
        {/* {categorias.loading2 ? (
          <p>Loading...</p>
        ) : categorias.error2 ? (
          <p>Error, try again</p>
        ) : (
          categorias.datos2.map(datos => {
            return <p>{datos.id}</p>;
          })
        )} */}
      </div>
    );
  }
}

const mapActionToProps = {
  fetchDog,
  fetchCategorias,
  fetchHome
};

const mapStateToProps = state => ({
  dog: state.dogs,
  categorias: state.catego
});

export default connect(mapStateToProps, mapActionToProps)(Prueba);
