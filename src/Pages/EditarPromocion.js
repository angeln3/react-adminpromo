import React, { Component } from "react";
import Fragment from "render-fragment";
//Date range picker
import DateRangePicker from "react-bootstrap-daterangepicker";
//Switchery
import Switch from "react-switchery";
//modals
import ActualizarPromo from '../utils/modals/ActualizarPromo';
import LLenarDatos from '../utils/modals/LLenarDatos';
//Formik
import { Formik, Form, Field, ErrorMessage } from 'formik';
//validaciones inputs
import {SchemaEditarPromo} from '../utils/validations/ProfileSchema';
//link
import {Link} from 'react-router-dom';
//jQuery
import $ from 'jquery';

export class EditarPromocion extends Component {
	constructor(props) {
		super(props);
		this.state = {
			NombrePromocion: "",
			FormaDePago: "",
			fechaEscogida: "",
			fechaInicialState: "",
			fechaFinalState: "",
			fechaActual: new Date(),
			Estatus: true
		};
	}

	ObtenerInfoInput = e => {
		const { name, value } = e.target;
		this.setState({
			[name]: value
		});
	};

	ObtenerFecha = (event, picker) => {
		var fechaInicial = picker.startDate.format("DD/MM/YYYY");
		var fechaFinal = picker.endDate.format("DD/MM/YYYY");

		this.setState({
			fechaEscogida: "" + fechaInicial + " - " + fechaFinal + "",
			fechaInicialState: fechaInicial,
			fechaFinalState: fechaFinal
		});
	};

	onChangeSwitch1 = value => {
		this.setState({
			Estatus: value
		});
	};

	onSubmit = (e) =>{
		e.preventDefault();

		const {
			NombrePromocion,
			FormaDePago,
			fechaEscogida,
			Estatus
		} = this.state;

		if(NombrePromocion == "" ||
		FormaDePago == "" ||
		fechaEscogida == "" ||
		Estatus == ""
		){
			$("#modalLLenarDatos").click();
		}else{
			var newDatos ={
				NombrePromocion,
				FormaDePago,
				fechaEscogida,
				Estatus
			}
			$('#modalDatosActualizados').click();
			console.log('Enviando...', newDatos);
		}

	}


	render() {
		const { fechaEscogida, fechaActual, Estatus } = this.state;

		return (
			<Fragment>
				<div className="right_col" role="main">
					<div className="">
						<div className="page-title">
							<div className="title_left">
								<h3>Editar Promoción </h3>
							</div>
						</div>
						<div className="clearfix"></div>

						<nav aria-label="breadcrumb">
							<ol className="breadcrumb">
								<li className="breadcrumb-item">
									<a href="#">Home</a>
								</li>
								<li className="breadcrumb-item">
									<a href="#">Administrador de bines</a>
								</li>
								<li className="breadcrumb-item">
									<a href="#">Buen Fin</a>
								</li>
								<li className="breadcrumb-item active">
									<a href="#">Edit</a>
								</li>
							</ol>
						</nav>

						<div className="row">
							<div className="col-md-12 col-sm-12">
								<div
									className="x_panel"
									style={{ boxShadow: "1px 4px 23px -13px black" }}
								>
									<h2 className="mb-4">Editar promoción Buen Fin</h2>

									<div className="clearfix"></div>

									<div className="col-md-12 col-sm-12 d-flex justify-content-center">
										<div
											className="x_panel col-md-10 mb-5"
											style={{ boxShadow: "1px 4px 23px -13px black" }}
										>
											<h2 className=" col-md-12 col-sm-12 breadcrumb">
												Datos de la promoción
											</h2>

											<div className="clearfix"></div>

											<div className="x_content">


											<Formik
												initialValues={{ NombrePromocion:'', FormaDePago: '', Fecha: '' }}
												validationSchema={SchemaEditarPromo}
												onSubmit={(values, { setSubmitting }) => {
													setTimeout(() => {
														console.log(values);
														alert(JSON.stringify(values, null, 2));
														setSubmitting(false);
													}, 400);
												}}
											>
												{({
													values,
													errors,
													touched,
													handleChange,
													handleBlur,
													handleSubmit,
													isSubmitting,
													isValid
													/* and other goodies */
												}) => (
													<form onSubmit={this.onSubmit}>
															<p>Nombre de la promoción*</p>
															<input
																type="text"
																name="NombrePromocion"
																className={errors.NombrePromocion && touched.NombrePromocion ? "form-control is-invalid transition" : "form-control mb-4"}
																onBlur={handleBlur}
																onChange={(event) => {this.ObtenerInfoInput(event); handleChange(event)}}
																value={values.NombrePromocion}
															/>
															{errors.NombrePromocion && touched.NombrePromocion && 
																<p className="text-danger mt-0 transition">{errors.NombrePromocion}</p>
															}

														<p>Forma de pago*</p>
														<select
															name="FormaDePago"
															className={errors.FormaDePago && touched.FormaDePago ? "form-control is-invalid transition" : "form-control mb-4"}
															onChange={(event) => {this.ObtenerInfoInput(event); handleChange(event)}}
															onBlur={handleBlur}
															value={values.FormaDePago}
														>
															<option value="">
																Seleccione uno
															</option>
															<option>Tarjetas de Crédito o Débito</option>
															<option>
																Paypal (Tarjeta de crédito o débito)
															</option>
														</select>
														{errors.FormaDePago && touched.FormaDePago && 
															<p className="text-danger mt-0 transition">{errors.FormaDePago}</p>
														}

														
														<p>Vigencia*</p>
														<DateRangePicker
															startDate={fechaActual}
															onApply={this.ObtenerFecha}
															minDate={fechaActual}
														>
															<input
																id="fecha"
																name="Fecha"
																onChange={handleChange}
																onBlur={handleBlur}    
																style={{ width: "300px" }}
																className={fechaEscogida == '' ? "form-control is-invalid transition" : "form-control mb-4"}
																value={fechaEscogida}
															/>
														</DateRangePicker>
														{fechaEscogida == '' ?
															<p className="text-danger mt-0 transition">Campo requerido</p> : null
														}


														<div className="col-md-12 col-sm-3 mb-5 pl-0">
															<p className="col-md-1 mr-2">Estado*</p>

															<Switch
																className="switch-class"
																onChange={this.onChangeSwitch1}
																options={{
																	color: "rgb(38, 185, 154)",
																	size: "small"
																}}
																checked={Estatus}
															/>
														</div>

														<div className="col-md-12 col-sm-3 text-center mb-5">

															<Link className="btn" to="/">Cancelar</Link>

															<button
																disabled={isSubmitting}
																className="btn btn-success"
																type="submit"
															>
																Actualizar
															</button>

															<button
																id="modalLLenarDatos"
																hidden
																data-toggle="modal"
																data-target=".modal-LLenarDatosEditar"
															>		
															</button>

															<button
																id="modalDatosActualizados"
																hidden
																data-toggle="modal"
																data-target=".actualizar-promo"
															>		
															</button>
															
														</div>														
													</form>
												)}
											</Formik>

											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
				{/* modal */}
				<ActualizarPromo clase="modal fade actualizar-promo" />
				<LLenarDatos clase="modal fade modal-LLenarDatosEditar" />
				{/* /modal */}

			</Fragment>
		);
	}
}

export default EditarPromocion;
