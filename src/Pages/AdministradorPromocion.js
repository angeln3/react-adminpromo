import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import $ from 'jquery';
import Fragment from 'render-fragment';
import VerDetalles from '../utils/modals/VerDetalles';
import EliminarPromo from '../utils/modals/EliminarPromo';

//Switchery
import Switch from 'react-switchery-component';

export class AdministradorPromocion extends Component {

  constructor(props) {
    super(props);
    this.state = {isChecked: false};
  }

  componentDidMount() {
    $('#datatable').DataTable();
  }
 
  handleChange = (e) => {
    const {name, checked} = e.target;
    console.log(name , checked);
    this.setState({isChecked: checked});
  }
  

  render() {

    const {isChecked} = this.state;

    return (
      <Fragment>
        <div className="right_col" role="main">
          <div className="">
            <div className="page-title">
              <div className="title_left">
                <h3>Administrador de promociones </h3>

              </div>

            </div>



            <div className="clearfix"></div>

            <nav aria-label="breadcrumb">
              <ol className="breadcrumb">
                <li className="breadcrumb-item"><a href="#">Home</a></li>
                <li className="breadcrumb-item active"><a href="#">Promociones</a></li>
              </ol>
            </nav>

            <div className="row">
              <div className="col-md-12 col-sm-12  ">
                <div className="x_panel" style={{ boxShadow: "1px 4px 23px -13px black" }}>


                  <h2 className="col-md-12 col-sm-12 mb-3">¿Desea agregar una nueva promoción?</h2>

                  <div className="col-md-4 col-sm-4 text-center">
                    <a className="btn btn-success" href="/CrearPromocion">Crear promoción </a>
                  </div>

                  <div className="clearfix"></div>
                  <div className="ln_solid"></div>


                  <div className="x_content">
                    <div className="row">
                      <div className="col-sm-12">
                        <div className="x_panel" style={{ boxShadow: "1px 4px 23px -13px black" }}>

                          <h2 className="mb-4 mt-4 col-md-12 col-sm-12 breadcrumb">Promociones</h2>





                          <div className="card-box table-responsive mt-4">

                            <table id="datatable" className="table table-striped  bulk_action">
                              <thead>
                                <tr className="headings">

                                  <th className="column-title">ID </th>
                                  <th className="column-title">Nombre de la promoción </th>
                                  <th className="column-title">Vigencia </th>
                                  <th className="column-title">Estatus</th>
                                  <th className="column-title"></th>

                                </tr>
                              </thead>

                              <tbody>

                                <tr className="even pointer">

                                  <td className=" ">700</td>
                                  <td className=" ">Buen fin</td>
                                  <td className=" ">01/01/2020 - 01/25/2020 </td>
                                  <td className="text-center">

                                    <Switch
                                      color="#26B99A"
                                      name="700"
                                      checked={isChecked}
                                      onChange={(e) => this.handleChange(e)}
                                      />
                                    
                                    <div className="d-none">activas</div>
                                  </td>
                                  <td className=" ">

                                    <div className="btn-group">
                                      <button type="button" className="btn btn-primary dropdown-toggle" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                        Acciones
                                                </button>
                                      <div className="dropdown-menu">
                                        <a className="dropdown-item" data-toggle="modal" data-target=".bs-example-modal-lg"><i className="fa fa-eye"></i>&nbsp;  Ver detalles</a>

                                        <Link to="/Editar" className="dropdown-item"><i className="fa fa-edit"></i>&nbsp;  Editar</Link>

                                        <a className="dropdown-item" data-toggle="modal" data-target=".modal-eliminar"><i className="fa fa-trash"></i>&nbsp; Eliminar </a>

                                        <div className="dropdown-divider"></div>
                                        <Link to="/AdministardorDeBines" className="dropdown-item" ><i className="fa fa-credit-card"></i>&nbsp; Administrar bines</Link>
                                      </div>
                                    </div>

                                  </td>

                                </tr>

                                <tr className="even pointer">

                                  <td className=" ">701</td>
                                  <td className=" ">Sanborns</td>
                                  <td className=" ">02/01/2020 - 01/25/2020 </td>
                                  <td className="text-center ">

                                    <Switch
                                      color="#26B99A"
                                      name="701"
                                      checked={isChecked}
                                      onChange={(e) => this.handleChange(e)}
                                      />

                                    <div className="d-none">inactivo</div>
                                  </td>
                                  <td className=" ">

                                   <div className="btn-group">
                                      <button type="button" className="btn btn-primary dropdown-toggle" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                        Acciones
                                                </button>
                                      <div className="dropdown-menu">
                                        <a className="dropdown-item" data-toggle="modal" data-target=".bs-example-modal-lg"><i className="fa fa-eye"></i>&nbsp;  Ver detalles</a>

                                        <Link to="/Editar" className="dropdown-item"><i className="fa fa-edit"></i>&nbsp;  Editar</Link>

                                        <a className="dropdown-item" data-toggle="modal" data-target=".modal-eliminar"><i className="fa fa-trash"></i>&nbsp; Eliminar </a>

                                        <div className="dropdown-divider"></div>
                                        <Link to="/AdministardorDeBines" className="dropdown-item" ><i className="fa fa-credit-card"></i>&nbsp; Administrar bines</Link>
                                      </div>
                                    </div>

                                  </td>

                                </tr>


                                <tr className="even pointer">

                                  <td className=" ">702</td>
                                  <td className=" ">Claroshop</td>
                                  <td className=" ">03/01/2020 - 01/25/2020 </td>
                                  <td className="text-center">

                                    <Switch
                                      color="#26B99A"
                                      name="702"
                                      checked={isChecked}
                                      onChange={(e) => this.handleChange(e)}
                                      />

                                    <div className="d-none">activas</div>
                                  </td>
                                  <td className=" ">

                                    <div className="btn-group">
                                      <button type="button" className="btn btn-primary dropdown-toggle" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                        Acciones
                                                </button>
                                      <div className="dropdown-menu">
                                        <a className="dropdown-item" data-toggle="modal" data-target=".bs-example-modal-lg"><i className="fa fa-eye"></i>&nbsp;  Ver detalles</a>

                                        <Link to="/Editar" className="dropdown-item"><i className="fa fa-edit"></i>&nbsp;  Editar</Link>

                                        <a className="dropdown-item" data-toggle="modal" data-target=".modal-eliminar"><i className="fa fa-trash"></i>&nbsp; Eliminar </a>

                                        <div className="dropdown-divider"></div>
                                        <Link to="/AdministardorDeBines" className="dropdown-item" ><i className="fa fa-credit-card"></i>&nbsp; Administrar bines</Link>
                                      </div>
                                    </div>

                                  </td>

                                </tr>

                                <tr className="even pointer">

                                  <td className=" ">703</td>
                                  <td className=" ">Sears</td>
                                  <td className=" ">04/01/2020 - 01/25/2020 </td>
                                  <td className="text-center ">
                                     <Switch
                                      color="#26B99A"
                                      name="703"
                                      checked={isChecked}
                                      onChange={(e) => this.handleChange(e)}
                                      />
                                    <div className="d-none">inactivo</div>
                                  </td>
                                  <td className=" ">
                                    <div className="btn-group">
                                      <button type="button" className="btn btn-primary dropdown-toggle" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                        Acciones
                                                </button>
                                      <div className="dropdown-menu">
                                        <a className="dropdown-item" data-toggle="modal" data-target=".bs-example-modal-lg"><i className="fa fa-eye"></i>&nbsp;  Ver detalles</a>

                                        <Link to="/Editar" className="dropdown-item"><i className="fa fa-edit"></i>&nbsp;  Editar</Link>

                                        <a className="dropdown-item" data-toggle="modal" data-target=".modal-eliminar"><i className="fa fa-trash"></i>&nbsp; Eliminar </a>

                                        <div className="dropdown-divider"></div>
                                        <Link to="/AdministardorDeBines" className="dropdown-item" ><i className="fa fa-credit-card"></i>&nbsp; Administrar bines</Link>
                                      </div>
                                    </div>

                                  </td>

                                </tr>

                                <tr className="even pointer">

                                  <td className=" ">704</td>
                                  <td className=" ">Sears</td>
                                  <td className=" ">22/01/2019 - 15/03/2019 </td>
                                  <td className="text-center ">
                                    <Switch
                                      color="#26B99A"
                                      name="704"
                                      checked={isChecked}
                                      onChange={(e) => this.handleChange(e)}
                                      />
                                    <div className="d-none">inactivo</div>
                                    <div className="d-none">vencida</div>
                                  </td>
                                  <td className=" ">
                                    <div className="btn-group">
                                      <button type="button" className="btn btn-primary dropdown-toggle" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                        Acciones
                                                </button>
                                      <div className="dropdown-menu">
                                        <a className="dropdown-item" data-toggle="modal" data-target=".bs-example-modal-lg"><i className="fa fa-eye"></i>&nbsp;  Ver detalles</a>

                                        <Link to="/Editar" className="dropdown-item"><i className="fa fa-edit"></i>&nbsp;  Editar</Link>

                                        <a className="dropdown-item" data-toggle="modal" data-target=".modal-eliminar"><i className="fa fa-trash"></i>&nbsp; Eliminar </a>

                                        <div className="dropdown-divider"></div>
                                        <Link to="/AdministardorDeBines" className="dropdown-item" ><i className="fa fa-credit-card"></i>&nbsp; Administrar bines</Link>
                                      </div>
                                    </div>

                                  </td>

                                </tr>

                              </tbody>
                            </table>
                          </div>

                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* modal */}
        <VerDetalles />
        <EliminarPromo />
        {/* /modal */}
      </Fragment>
    )
  }
}

export default AdministradorPromocion;
