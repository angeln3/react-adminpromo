import React from "react";
//Switchery
import Switch from "react-switchery";
// Icheck
import { Checkbox, Radio } from "react-icheck";
//Date range picker
import DateRangePicker from "react-bootstrap-daterangepicker";
//jquery
import $ from "jquery";
//componentes
import ListaBlanca from "../components/CrearPromocion/ListaBlanca";
import ListaNegra from "../components/CrearPromocion/ListaNegra";
//import fracment
import Fragment from "render-fragment";
//import link
import { Link } from "react-router-dom";
//modals
import PromocionCreada from "../utils/modals/PromocionCreada";
import LLenarDatos from "../utils/modals/LLenarDatos";
//Formik
import { Formik, Form, Field, ErrorMessage } from "formik";
//schema de validaciones
import { SchemaCrearPromo_1 } from "../utils/validations/ProfileSchema";
import { SchemaCrearPromo_2 } from "../utils/validations/ProfileSchema";
import { SchemaCrearPromo_3 } from "../utils/validations/ProfileSchema";
//redux stuff
import { connect } from "react-redux";
//import { getCategorias } from "../redux/actions/dataActions";

class CrearPromocion extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      NombrePromocion: "",
      fechaEscogida: "",
      Origen: "",
      Estatus: false,
      FormaDePago: "",
      TipoDescuento: "Porcentaje %",
      Descuento: "",
      montoMin: "",
      montoMax: "",
      fechaInicialState: "",
      fechaFinalState: "",
      CAT: "",
      Lineamientos: "",
      fechaActual: new Date(),
      Bancos: [],
      Msi: [],

      val1afirme: false,
      val2afirme: false,
      val1banamex: false,
      arr: [],
      SoloMsi: [
        {
          Banco: "afirme",
          msi: [
            {
              mes: "1"
            },
            {
              mes: "2"
            }
          ]
        },
        {
          Banco: "bancomer",
          msi: [
            {
              mes: "3"
            },
            {
              mes: "4"
            }
          ]
        }
      ],
      CopiaMsi: []
    };
  }

  ObtenerCatego = () => {
    //this.props.getCategorias();
  };

  onChangeCheckboxBanck = e => {
    //se optiene el nombre del banco
    var idBanco = e.target.id;
    var idBancoGato = "#" + idBanco + "";
    var bancoState = "BNC" + idBanco;
    var primerValorMSI = "#val1" + idBanco + "";
    var primerValorMSIstate = "val1" + idBanco;
    var idMSI = "#Meses" + idBanco + "";
    var valor = $(idBancoGato).prop("checked");

    //se asigna al state
    // this.setState((e)=>({
    //     Bancos:{...e.Bancos,
    //     [bancoState]: valor
    //     }
    // }));

    // si esta seleccionado
    if (valor) {
      //se abren los MSI
      $(idMSI).slideDown();
      //se activa el primer valor
      $(primerValorMSI).prop("checked", true);
      var valorval1 = $(primerValorMSI).prop("checked");
      //se asigna el primer valor al state

      // this.setState((e) => ({
      //     [bancoState]: {
      //         [bancoState]: valor,
      //         msi: {
      //             [primerValorMSIstate]: valorval1
      //         }
      //     }
      // }));

      this.setState({
        ["Msi" + idBanco]: [
          {
            [primerValorMSIstate]: valorval1
          }
        ]
      });

      this.setState(e => ({
        Msi: [
          ...e.Msi,
          {
            Banco: idBanco,
            ["Msi" + idBanco]: [
              {
                [primerValorMSIstate]: valorval1
              }
            ]
          }
        ]
      }));

      this.setState(e => ({
        Bancos: [
          ...e.Bancos,
          {
            Banco: idBanco,
            ["Msi" + idBanco]: [
              {
                [primerValorMSIstate]: valorval1
              }
            ]
          }
        ]
      }));
      // this.setState({
      //     [primerValorMSIstate]: valorval1
      // });

      //si lo deselecciona
    } else {
      // se cierran los MSI
      $(idMSI).slideUp();
      //encontrar el index e eliminarlo de la lista
      const filterData = this.state.Bancos.filter(
        banco => banco.Banco !== idBanco
      );
      //var index = this.state.Bancos.findIndex(banco => banco.Banco !== idBanco);
      this.setState({
        Bancos: filterData
      });
      console.log(filterData);
      //console.log('INDEX elim',index);
      //this.state.Bancos.splice(index,1);

      this.setState(e => ({
        Msi: [
          {
            Banco: idBanco,
            ["Msi" + idBanco]: []
          }
        ]
      }));

      // se ponen todos los MSI en falso
      for (var i = 1; i <= 10; i++) {
        //Se quita del state'#val'+i+''+idBanco+'';
        // this.setState({
        //     ['val'+i+idBanco]: false
        // });
        //se quitan select
        $("#val" + i + "" + idBanco + "").prop("checked", false);
      }
    }
  };

  onChangeCheckboxBanckMeses = e => {
    const { name } = e.target;

    var idBanco = name;
    //se optiene el nombre del MSI
    var msiId = e.target.id;
    var idBancoGato = "#" + msiId + "";
    var valor = $(idBancoGato).prop("checked");
    let msiName = "Msi" + name;

    console.log("valor de msi", valor);

    if (valor) {
      this.setState(e => ({
        //agregar elementos a un array en el state
        ["Msi" + name]: [
          ...this.state["Msi" + name],
          {
            [msiId]: valor
          }
        ]
      }));

      //primero eliminamos el objeto que se creo y despues lo agregamos
      //encontrar el index e eliminarlo de la lista
      const filterData = this.state.Msi.filter(
        banco => banco.Banco !== idBanco
      );
      //var index = this.state.Bancos.findIndex(banco => banco.Banco !== idBanco);
      this.setState({
        Msi: filterData
      });

      /////////////////
      this.setState(e => ({
        Msi: [
          ...e.Msi,
          {
            Banco: idBanco,
            ["Msi" + name]: [
              ...this.state["Msi" + name],
              {
                [msiId]: valor
              }
            ]
          }
        ]
      }));

      this.setState(e => ({
        CopiaMsi: [
          ...e.CopiaMsi,
          {
            Banco: idBanco,
            msi: [
              //...this.state[msi],
              {
                mes: msiId
              }
            ]
          }
        ]
      }));
    } else {
      // var msiBanco = "Msi" + name;
      // const filterData = this.state.msiBanco.filter(
      //   msi => msi.Banco !== idBanco
      // );
      console.log("entra en falso");

      this.setState(e => ({
        //agregar elementos a un array en el state
        ["Msi" + name]: [
          ...this.state["Msi" + name],
          {
            [msiId]: valor
          }
        ]
      }));

      //primero eliminamos el objeto que se creo y despues lo agregamos
      //encontrar el index e eliminarlo de la lista
      const filterData = this.state.Msi.filter(
        banco => banco.Banco !== idBanco
      );
      //var index = this.state.Bancos.findIndex(banco => banco.Banco !== idBanco);
      this.setState({
        Msi: filterData
      });

      this.setState(e => ({
        Msi: [
          ...e.Msi,
          {
            Banco: idBanco,
            ["Msi" + name]: [
              ...this.state["Msi" + name],
              {
                [msiId]: valor
              }
            ]
          }
        ]
      }));
    }

    // this.setState(e => ({
    //   Msi: [
    //     ...e.Msi,
    //     {
    //       Banco: idBanco,
    //       ["Msi" + name]: [
    //         ...this.state["Msi" + name],
    //         {
    //           [msiId]: valor
    //         }
    //       ]
    //     }
    //   ]
    // }));

    // this.setState(e => ({
    //   Bancos: [
    //     {
    //       Banco: idBanco,
    //       ["Msi" + name]: [
    //         ...this.state["Msi" + name],
    //         {
    //           [msiId]: valor
    //         }
    //       ]
    //     }
    //   ]
    // }));
  };

  ObtenerFecha = (event, picker) => {
    console.log(picker.startDate.format("DD/MM/YYYY"));

    var fechaInicial = picker.startDate.format("DD/MM/YYYY");
    var fechaFinal = picker.endDate.format("DD/MM/YYYY");

    console.log("" + fechaInicial + " - " + fechaFinal + "");

    this.setState({
      fechaEscogida: "" + fechaInicial + " - " + fechaFinal + "",
      fechaInicialState: fechaInicial,
      fechaFinalState: fechaFinal
    });
  };

  ObtenerInfoInput = e => {
    const { name, value } = e.target;

    console.log(name, value);

    if (name == "TipoDescuento" && value == "Sin descuento") {
      this.setState({
        Descuento: 0
      });
    }

    this.setState({
      [name]: value
    });
  };

  onChangeSwitch1 = value => {
    $("#check2").prop("checked", value);
    this.setState({
      Estatus: value
    });
  };

  componentWillMount() {
    //location.reload();
  }
  componentDidMount() {
    //this.props.getCategorias();
  }

  enviarDatos = e => {
    e.preventDefault();

    const {
      NombrePromocion,
      fechaEscogida,
      Origen,
      Estatus,
      FormaDePago,
      TipoDescuento,
      Descuento,
      montoMin,
      montoMax,
      CAT,
      Lineamientos,
      Bancos
    } = this.state;

    if (
      NombrePromocion == "" ||
      fechaEscogida == "" ||
      Origen == "" ||
      FormaDePago == "" ||
      montoMin == "" ||
      montoMax == "" ||
      TipoDescuento == ""
    ) {
      $("#modalLLenarDatos").click();
    } else {
      var datos = {
        NombrePromocion,
        fechaEscogida,
        Origen,
        Estatus,
        FormaDePago,
        TipoDescuento,
        Descuento,
        montoMin,
        montoMax,
        CAT,
        Lineamientos,
        Bancos
      };

      $("#modalDatosExitosos").click();

      console.log("Crear Promocion datos...", datos);
      console.log("state global", this.state);
    }
  };

  render() {
    const {
      NombrePromocion,
      fechaEscogida,
      Origen,
      Estatus,
      FormaDePago,
      TipoDescuento,
      Descuento,
      montoMin,
      montoMax,
      fechaActual,
      Bancos,
      val1afirme,
      val2afirme,
      val1banamex,
      Msi,
      SoloMsi
    } = this.state;

    const { datosCategorias } = this.props;

    //console.log(datosCategorias);

    return (
      <Fragment>
        <div className="right_col" role="main">
          <div className="">
            <div className="page-title">
              <div className="title_left">
                <h3>Crear promoción</h3>
              </div>
            </div>
            <div className="clearfix"></div>

            <nav aria-label="breadcrumb">
              <ol className="breadcrumb">
                <li className="breadcrumb-item">
                  <a href="#">Home</a>
                </li>
                <li className="breadcrumb-item">
                  <Link to="/AdministardorDePromociones">
                    Admin Promociones
                  </Link>
                </li>
                <li className="breadcrumb-item active">
                  <a href="#">Nuevo</a>
                </li>
              </ol>
            </nav>

            <div className="row">
              <div className="col-md-12 col-sm-12 ">
                <div
                  className="x_panel"
                  style={{ boxShadow: "1px 4px 23px -13px black" }}
                >
                  <div className="x_title">
                    <h2>TDC / TDD </h2>

                    <div className="clearfix"></div>
                  </div>
                  <div className="x_content">
                    {/* Smart Wizard */}

                    <div id="wizard" className="form_wizard wizard_horizontal">
                      <ul className="wizard_steps">
                        <li>
                          <a href="#step-1">
                            <span className="step_no">1</span>
                            <span className="step_descr">
                              Paso 1<br />
                              <small>Datos generales</small>
                            </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-2">
                            <span className="step_no">2</span>
                            <span className="step_descr">
                              Paso 2<br />
                              <small>Formas de pago</small>
                            </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-3">
                            <span className="step_no">3</span>
                            <span className="step_descr">
                              Paso 3<br />
                              <small>Bancos y tiendas participantes</small>
                            </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-4">
                            <span className="step_no">4</span>
                            <span className="step_descr">
                              Paso 4<br />
                              <small>Validar promoción</small>
                            </span>
                          </a>
                        </li>
                      </ul>
                      <div>
                        <div id="step-1">
                          <Formik
                            initialValues={{
                              NombrePromocion: "",
                              fecha: "",
                              Origen: ""
                            }}
                            validationSchema={SchemaCrearPromo_1}
                            onSubmit={(values, { setSubmitting }) => {
                              setTimeout(() => {
                                console.log(values);
                                alert(JSON.stringify(values, null, 2));
                                setSubmitting(false);
                              }, 400);
                            }}
                          >
                            {({
                              values,
                              errors,
                              touched,
                              handleChange,
                              handleBlur,
                              handleSubmit,
                              isSubmitting,
                              isValid
                              /* and other goodies */
                            }) => (
                              <form onSubmit={this.onSubmit}>
                                <div className="form-group row">
                                  <label className="col-form-label col-md-3 col-sm-3 label-align">
                                    Nombre de la promoción{" "}
                                    <span className="required">*</span>
                                  </label>
                                  <div className="col-md-6 col-sm-3 ">
                                    <input
                                      type="text"
                                      name="NombrePromocion"
                                      required
                                      className={
                                        errors.NombrePromocion &&
                                        touched.NombrePromocion
                                          ? "form-control is-invalid transition"
                                          : "form-control"
                                      }
                                      onBlur={handleBlur}
                                      onChange={event => {
                                        this.ObtenerInfoInput(event);
                                        handleChange(event);
                                      }}
                                      value={values.NombrePromocion}
                                    />
                                    {errors.NombrePromocion &&
                                      touched.NombrePromocion && (
                                        <p className="text-danger mt-0 mb-0 transition">
                                          {errors.NombrePromocion}
                                        </p>
                                      )}
                                  </div>
                                </div>

                                <div className="form-group row">
                                  <label
                                    className="col-form-label col-md-3 col-sm-3 label-align"
                                    for="last-name"
                                  >
                                    Vigencia <span className="required">*</span>
                                  </label>
                                  <div className="col-md-3 col-sm-3 ">
                                    <div className="control-group ">
                                      <div className="controls">
                                        <div className="input-prepend input-group">
                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                          Fecha inicio - Fecha fin
                                          <DateRangePicker
                                            startDate={fechaActual}
                                            onApply={this.ObtenerFecha}
                                            minDate={fechaActual}
                                          >
                                            <input
                                              name="fecha"
                                              onChange={handleChange}
                                              onBlur={handleBlur}
                                              style={{ width: "300px" }}
                                              className={
                                                fechaEscogida == ""
                                                  ? "form-control is-invalid transition"
                                                  : "form-control mb-4"
                                              }
                                              value={fechaEscogida}
                                            />
                                          </DateRangePicker>
                                          {fechaEscogida == "" ? (
                                            <p className="text-danger mt-0 mb-0 transition">
                                              Campo requerido
                                            </p>
                                          ) : null}
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div className="form-group row">
                                  <label
                                    className="col-form-label col-md-3 col-sm-3 label-align"
                                    for="last-name"
                                  >
                                    Origen <span className="required">*</span>
                                  </label>
                                  <div className="col-md-3 col-sm-3 ">
                                    <select
                                      name="Origen"
                                      className={
                                        errors.Origen && touched.Origen
                                          ? "form-control is-invalid transition"
                                          : "form-control"
                                      }
                                      onChange={event => {
                                        this.ObtenerInfoInput(event);
                                        handleChange(event);
                                      }}
                                      onBlur={handleBlur}
                                      value={values.Origen}
                                      required
                                    >
                                      <option value="">Seleccione uno</option>
                                      <option>Claroshop.com</option>
                                      <option>App Claroshop</option>
                                      <option>Ambos</option>
                                    </select>
                                    {errors.Origen && touched.Origen && (
                                      <p className="text-danger mt-0 mb-0 transition">
                                        {errors.Origen}
                                      </p>
                                    )}
                                  </div>
                                </div>

                                <div className="form-group row mt-4">
                                  <label
                                    className="col-form-label col-md-3 col-sm-3 label-align"
                                    for="last-name"
                                  >
                                    Estatus <span className="required">*</span>
                                  </label>
                                  <div className="col-md-3 col-sm-3 mt-1 ml-1">
                                    <Switch
                                      className="switch-class"
                                      onChange={this.onChangeSwitch1}
                                      options={{
                                        color: "rgb(38, 185, 154)",
                                        size: "small"
                                      }}
                                      checked={Estatus}
                                    />
                                  </div>
                                </div>
                              </form>
                            )}
                          </Formik>
                        </div>

                        <div id="step-2">
                          <Formik
                            initialValues={{
                              FormaDePago: "",
                              TipoDescuento: "Porcentaje %",
                              Descuento: "",
                              montoMin: "",
                              montoMax: ""
                            }}
                            validationSchema={SchemaCrearPromo_2}
                            onSubmit={(values, { setSubmitting }) => {
                              setTimeout(() => {
                                console.log(values);
                                alert(JSON.stringify(values, null, 2));
                                setSubmitting(false);
                              }, 400);
                            }}
                          >
                            {({
                              values,
                              errors,
                              touched,
                              handleChange,
                              handleBlur,
                              handleSubmit,
                              isSubmitting,
                              isValid
                              /* and other goodies */
                            }) => (
                              <form onSubmit={this.onSubmit}>
                                <div className="form-group row">
                                  <label className="col-form-label col-md-3 col-sm-3 label-align">
                                    Forma de pago{" "}
                                    <span className="required">*</span>
                                  </label>
                                  <div className="col-md-6 col-sm-6 ">
                                    <select
                                      name="FormaDePago"
                                      className={
                                        errors.FormaDePago &&
                                        touched.FormaDePago
                                          ? "form-control is-invalid transition"
                                          : "form-control"
                                      }
                                      onChange={event => {
                                        this.ObtenerInfoInput(event);
                                        handleChange(event);
                                      }}
                                      onBlur={handleBlur}
                                      value={values.FormaDePago}
                                      required
                                    >
                                      <option value="">Seleccione uno</option>
                                      <option>
                                        Tarjetas de Crédito o Débito
                                      </option>
                                      <option>
                                        Paypal (Tarjeta de crédito o débito)
                                      </option>
                                    </select>
                                    {errors.FormaDePago &&
                                      touched.FormaDePago && (
                                        <p className="text-danger mt-0 mb-0 transition">
                                          {errors.FormaDePago}
                                        </p>
                                      )}
                                  </div>
                                </div>

                                <div className="form-group row">
                                  <label
                                    className="col-form-label col-md-3 col-sm-3 label-align"
                                    for="last-name"
                                  >
                                    Tipo descuento{" "}
                                    <span className="required">*</span>
                                  </label>
                                  <div className="col-md-3 col-sm-3 ">
                                    <select
                                      id="tipodescuento"
                                      name="TipoDescuento"
                                      className={
                                        errors.TipoDescuento &&
                                        touched.TipoDescuento
                                          ? "form-control is-invalid transition"
                                          : "form-control"
                                      }
                                      onChange={event => {
                                        this.ObtenerInfoInput(event);
                                        handleChange(event);
                                      }}
                                      onBlur={handleBlur}
                                      value={values.TipoDescuento}
                                      required
                                    >
                                      <option>Porcentaje %</option>
                                      <option>Pesos (MX)</option>
                                      <option>Sin descuento</option>
                                    </select>
                                    {errors.TipoDescuento &&
                                      touched.TipoDescuento && (
                                        <p className="text-danger mt-0 mb-0 transition">
                                          {errors.TipoDescuento}
                                        </p>
                                      )}
                                  </div>
                                </div>

                                <div className="form-group row">
                                  <label
                                    className="col-form-label col-md-3 col-sm-3 label-align"
                                    for="last-name"
                                  >
                                    Descuento{" "}
                                    <span className="required">*</span>
                                  </label>
                                  <div
                                    className="col-md-3 col-sm-3"
                                    id="descuentotxt"
                                  >
                                    <input
                                      id="Descuento"
                                      name="Descuento"
                                      className={
                                        errors.Descuento && touched.Descuento
                                          ? "form-control is-invalid transition"
                                          : "form-control"
                                      }
                                      onChange={event => {
                                        this.ObtenerInfoInput(event);
                                        handleChange(event);
                                      }}
                                      onBlur={handleBlur}
                                      value={values.Descuento}
                                      disabled={
                                        TipoDescuento == "Sin descuento"
                                          ? true
                                          : false
                                      }
                                      value={
                                        TipoDescuento == "Sin descuento"
                                          ? 0
                                          : null
                                      }
                                      required
                                    />
                                    {errors.Descuento && touched.Descuento && (
                                      <p className="text-danger mt-0 mb-0 transition">
                                        {errors.Descuento}
                                      </p>
                                    )}
                                  </div>
                                </div>
                                <div className="form-group row">
                                  <p className="col-form-label col-md-12 col-sm-3 text-center mt-4">
                                    Monto (MXN)
                                  </p>
                                </div>

                                <div className="form-group row">
                                  <label
                                    className="col-form-label col-md-3 col-sm-3 label-align"
                                    for="last-name"
                                  ></label>
                                  <div className="col-md-3 col-sm-3 ">
                                    Monto min{" "}
                                    <span className="required">*</span>
                                    <input
                                      name="montoMin"
                                      className={
                                        errors.montoMin && touched.montoMin
                                          ? "form-control is-invalid transition"
                                          : "form-control"
                                      }
                                      onChange={event => {
                                        this.ObtenerInfoInput(event);
                                        handleChange(event);
                                      }}
                                      onBlur={handleBlur}
                                      value={values.montoMin}
                                      required
                                    />
                                    {errors.montoMin && touched.montoMin && (
                                      <p className="text-danger mt-0 mb-0 transition">
                                        {errors.montoMin}
                                      </p>
                                    )}
                                  </div>
                                  <label
                                    className="col-form-label col-md-1 col-sm-1 label-align"
                                    for="last-name"
                                  ></label>
                                  <div className="col-md- col-sm-3 ">
                                    Monto max{" "}
                                    <span className="required">*</span>
                                    <input
                                      name="montoMax"
                                      className={
                                        errors.montoMax && touched.montoMax
                                          ? "form-control is-invalid transition"
                                          : "form-control"
                                      }
                                      onChange={event => {
                                        this.ObtenerInfoInput(event);
                                        handleChange(event);
                                      }}
                                      onBlur={handleBlur}
                                      value={values.montoMax}
                                      required
                                    />
                                    {errors.montoMax && touched.montoMax && (
                                      <p className="text-danger mt-0 mb-0 transition">
                                        {errors.montoMax}
                                      </p>
                                    )}
                                  </div>
                                </div>
                              </form>
                            )}
                          </Formik>
                        </div>

                        <div id="step-3">
                          <form className="form-horizontal form-label-left">
                            <div className="form-group row">
                              <div className="col-form-label col-md-3 col-sm-3 label-align">
                                <h4>
                                  Seleccione bancos participantes
                                  <span className="required">*</span>
                                </h4>
                              </div>
                              <div className="col-md-9">
                                {FormaDePago == "" ? (
                                  <p className="text-danger mt-0 mb-0 transition">
                                    *Debes seleccionar almenos una forma de pago
                                    para seleccionar un banco
                                  </p>
                                ) : null}
                              </div>
                            </div>

                            {/* Para cada input agregar id de banco y támbien al menu despegable de meses, seguir la misma logica */}

                            {/* columna 1 bancos */}
                            <div className="form-group row">
                              <label className="col-form-label col-md-3 col-sm-3 label-align"></label>

                              <div className="col-md-2 col-sm-2">
                                <Checkbox
                                  checkboxClass="icheckbox_flat-green"
                                  increaseArea="20%"
                                  label=" AFIRME"
                                  id="afirme"
                                  class="flat"
                                  onChange={this.onChangeCheckboxBanck}
                                  disabled={FormaDePago == "" ? true : false}
                                />

                                {/* este es el menu despegable de meses */}
                                <div
                                  id="Mesesafirme"
                                  style={{ display: "none" }}
                                >
                                  <div className="col-md-12">
                                    <p className="mb-1">MSI</p>
                                    <div className="ln_solid mt-0 mb-1 mr-4"></div>
                                  </div>

                                  <div className="col-md-5 col-sm-3">
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val1afirme"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="afirme"
                                        />{" "}
                                        1
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val2afirme"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="afirme"
                                        />{" "}
                                        3
                                      </label>
                                    </div>

                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val3afirme"
                                          value="6"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="afirme"
                                        />{" "}
                                        6
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val4afirme"
                                          value="9"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="afirme"
                                        />{" "}
                                        9
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val5afirme"
                                          value="12"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="afirme"
                                        />{" "}
                                        12
                                      </label>
                                    </div>
                                  </div>

                                  <div className="col-md-5 col-sm-5">
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val6afirme"
                                          value="13"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="afirme"
                                        />{" "}
                                        13
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val7afirme"
                                          value="15"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="afirme"
                                        />{" "}
                                        15
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val8afirme"
                                          value="18"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="afirme"
                                        />{" "}
                                        18
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val9afirme"
                                          value="20"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="afirme"
                                        />{" "}
                                        20
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val10afirme"
                                          value="24"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="afirme"
                                        />{" "}
                                        24
                                      </label>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div className="col-md-2 col-sm-2">
                                <Checkbox
                                  checkboxClass="icheckbox_flat-green"
                                  increaseArea="20%"
                                  label=" BANAMEX"
                                  id="banamex"
                                  class="flat"
                                  onChange={this.onChangeCheckboxBanck}
                                  disabled={FormaDePago == "" ? true : false}
                                />

                                {/* este es el menu despegable de meses */}
                                <div
                                  id="Mesesbanamex"
                                  style={{ display: "none" }}
                                >
                                  <div className="col-md-12">
                                    <p className="mb-1">MSI</p>
                                    <div className="ln_solid mt-0 mb-1 mr-4"></div>
                                  </div>
                                  <div className="col-md-5 col-sm-3">
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val1banamex"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="banamex"
                                        />{" "}
                                        1
                                      </label>
                                    </div>

                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val2banamex"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="banamex"
                                        />{" "}
                                        3
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val3banamex"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="banamex"
                                        />{" "}
                                        6
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val4banamex"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="banamex"
                                        />{" "}
                                        9
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val5banamex"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="banamex"
                                        />{" "}
                                        12
                                      </label>
                                    </div>
                                  </div>

                                  <div className="col-md-5 col-sm-5">
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val6banamex"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="banamex"
                                        />{" "}
                                        13
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val7banamex"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="banamex"
                                        />{" "}
                                        15
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val8banamex"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="banamex"
                                        />{" "}
                                        18
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val9banamex"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="banamex"
                                        />{" "}
                                        20
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val10banamex"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="banamex"
                                        />{" "}
                                        24
                                      </label>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div className="col-md-2 col-sm-2">
                                <Checkbox
                                  checkboxClass="icheckbox_flat-green"
                                  increaseArea="20%"
                                  label=" BANCOMER"
                                  id="bancomer"
                                  class="flat"
                                  onChange={this.onChangeCheckboxBanck}
                                  disabled={FormaDePago == "" ? true : false}
                                />

                                {/* este es el menu despegable de meses */}
                                <div
                                  id="Mesesbancomer"
                                  style={{ display: "none" }}
                                >
                                  <div className="col-md-12">
                                    <p className="mb-1">MSI</p>
                                    <div className="ln_solid mt-0 mb-1 mr-4"></div>
                                  </div>
                                  <div className="col-md-5 col-sm-3">
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val1bancomer"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="bancomer"
                                        />{" "}
                                        1
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val2bancomer"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="bancomer"
                                        />{" "}
                                        3
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val3bancomer"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="bancomer"
                                        />{" "}
                                        6
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val4bancomer"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="bancomer"
                                        />{" "}
                                        9
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val5bancomer"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="bancomer"
                                        />{" "}
                                        12
                                      </label>
                                    </div>
                                  </div>

                                  <div className="col-md-5 col-sm-5">
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val6bancomer"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="bancomer"
                                        />{" "}
                                        13
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val7bancomer"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="bancomer"
                                        />{" "}
                                        15
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val8bancomer"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="bancomer"
                                        />{" "}
                                        18
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val9bancomer"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="bancomer"
                                        />{" "}
                                        20
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val10bancomer"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="bancomer"
                                        />{" "}
                                        24
                                      </label>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div className="col-md-2 col-sm-2">
                                <Checkbox
                                  checkboxClass="icheckbox_flat-green"
                                  increaseArea="20%"
                                  label=" BANORTE"
                                  id="banorte"
                                  class="flat"
                                  onChange={this.onChangeCheckboxBanck}
                                  disabled={FormaDePago == "" ? true : false}
                                />

                                {/* este es el menu despegable de meses */}
                                <div
                                  id="Mesesbanorte"
                                  style={{ display: "none" }}
                                >
                                  <div className="col-md-12">
                                    <p className="mb-1">MSI</p>
                                    <div className="ln_solid mt-0 mb-1 mr-4"></div>
                                  </div>
                                  <div className="col-md-5 col-sm-3">
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val1banorte"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="banorte"
                                        />{" "}
                                        1
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val2banorte"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="banorte"
                                        />{" "}
                                        3
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val3banorte"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="banorte"
                                        />{" "}
                                        6
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val4banorte"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="banorte"
                                        />{" "}
                                        9
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val5banorte"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="banorte"
                                        />{" "}
                                        12
                                      </label>
                                    </div>
                                  </div>

                                  <div className="col-md-5 col-sm-5">
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val6banorte"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="banorte"
                                        />{" "}
                                        13
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val7banorte"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="banorte"
                                        />{" "}
                                        15
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val8banorte"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="banorte"
                                        />{" "}
                                        18
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val9banorte"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="banorte"
                                        />{" "}
                                        20
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val10banorte"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="banorte"
                                        />{" "}
                                        24
                                      </label>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            {/* columna 2 bancos */}
                            <div className="form-group row">
                              <label className="col-form-label col-md-3 col-sm-3 label-align"></label>

                              <div className="col-md-2 col-sm-2">
                                <Checkbox
                                  checkboxClass="icheckbox_flat-green"
                                  increaseArea="20%"
                                  label=" HSBC"
                                  id="hsbc"
                                  class="flat"
                                  onChange={this.onChangeCheckboxBanck}
                                  disabled={FormaDePago == "" ? true : false}
                                />
                                {/* este es el menu despegable de meses */}
                                <div id="Meseshsbc" style={{ display: "none" }}>
                                  <div className="col-md-12">
                                    <p className="mb-1">MSI</p>
                                    <div className="ln_solid mt-0 mb-1 mr-4"></div>
                                  </div>
                                  <div className="col-md-5 col-sm-3">
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val1hsbc"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="hsbc"
                                        />{" "}
                                        1
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val2hsbc"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="hsbc"
                                        />{" "}
                                        3
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val3hsbc"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="hsbc"
                                        />{" "}
                                        6
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val4hsbc"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="hsbc"
                                        />{" "}
                                        9
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val5hsbc"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="hsbc"
                                        />{" "}
                                        12
                                      </label>
                                    </div>
                                  </div>

                                  <div className="col-md-5 col-sm-5">
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val6hsbc"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="hsbc"
                                        />{" "}
                                        13
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val7hsbc"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="hsbc"
                                        />{" "}
                                        15
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val8hsbc"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="hsbc"
                                        />{" "}
                                        18
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val9hsbc"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="hsbc"
                                        />{" "}
                                        20
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val10hsbc"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="hsbc"
                                        />{" "}
                                        24
                                      </label>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div className="col-md-2 col-sm-2">
                                <Checkbox
                                  checkboxClass="icheckbox_flat-green"
                                  increaseArea="20%"
                                  label=" INBURSA"
                                  id="inbursa"
                                  class="flat"
                                  onChange={this.onChangeCheckboxBanck}
                                  disabled={FormaDePago == "" ? true : false}
                                />
                                {/* este es el menu despegable de meses */}
                                <div
                                  id="Mesesinbursa"
                                  style={{ display: "none" }}
                                >
                                  <div className="col-md-12">
                                    <p className="mb-1">MSI</p>
                                    <div className="ln_solid mt-0 mb-1 mr-4"></div>
                                  </div>
                                  <div className="col-md-5 col-sm-3">
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val1inbursa"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="inbursa"
                                        />{" "}
                                        1
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val2inbursa"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="inbursa"
                                        />{" "}
                                        3
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val3inbursa"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="inbursa"
                                        />{" "}
                                        6
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val4inbursa"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="inbursa"
                                        />{" "}
                                        9
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val5inbursa"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="inbursa"
                                        />{" "}
                                        12
                                      </label>
                                    </div>
                                  </div>

                                  <div className="col-md-5 col-sm-5">
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val6inbursa"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="inbursa"
                                        />{" "}
                                        13
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val7inbursa"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="inbursa"
                                        />{" "}
                                        15
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val8inbursa"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="inbursa"
                                        />{" "}
                                        18
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val9inbursa"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="inbursa"
                                        />{" "}
                                        20
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val10inbursa"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="inbursa"
                                        />{" "}
                                        24
                                      </label>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div className="col-md-2 col-sm-2">
                                <Checkbox
                                  checkboxClass="icheckbox_flat-green"
                                  increaseArea="20%"
                                  label=" INVEX"
                                  id="invex"
                                  class="flat"
                                  onChange={this.onChangeCheckboxBanck}
                                  disabled={FormaDePago == "" ? true : false}
                                />
                                {/* este es el menu despegable de meses */}
                                <div
                                  id="Mesesinvex"
                                  style={{ display: "none" }}
                                >
                                  <div className="col-md-12">
                                    <p className="mb-1">MSI</p>
                                    <div className="ln_solid mt-0 mb-1 mr-4"></div>
                                  </div>
                                  <div className="col-md-5 col-sm-3">
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val1invex"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="invex"
                                        />{" "}
                                        1
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val2invex"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="invex"
                                        />{" "}
                                        3
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val3invex"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="invex"
                                        />{" "}
                                        6
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val4invex"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="invex"
                                        />{" "}
                                        9
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val5invex"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="invex"
                                        />{" "}
                                        12
                                      </label>
                                    </div>
                                  </div>

                                  <div className="col-md-5 col-sm-5">
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val6invex"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="invex"
                                        />{" "}
                                        13
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val7invex"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="invex"
                                        />{" "}
                                        15
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val8invex"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="invex"
                                        />{" "}
                                        18
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val9invex"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="invex"
                                        />{" "}
                                        20
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val10invex"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="invex"
                                        />{" "}
                                        24
                                      </label>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div className="col-md-2 col-sm-2">
                                <Checkbox
                                  checkboxClass="icheckbox_flat-green"
                                  increaseArea="20%"
                                  label=" SANTANDER"
                                  id="santander"
                                  class="flat"
                                  onChange={this.onChangeCheckboxBanck}
                                  disabled={FormaDePago == "" ? true : false}
                                />
                                {/* este es el menu despegable de meses */}
                                <div
                                  id="Mesessantander"
                                  style={{ display: "none" }}
                                >
                                  <div className="col-md-12">
                                    <p className="mb-1">MSI</p>
                                    <div className="ln_solid mt-0 mb-1 mr-4"></div>
                                  </div>
                                  <div className="col-md-5 col-sm-3">
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val1santander"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="santander"
                                        />{" "}
                                        1
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val2santander"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="santander"
                                        />{" "}
                                        3
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val3santander"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="santander"
                                        />{" "}
                                        6
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val4santander"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="santander"
                                        />{" "}
                                        9
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val5santander"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="santander"
                                        />{" "}
                                        12
                                      </label>
                                    </div>
                                  </div>

                                  <div className="col-md-5 col-sm-5">
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val6santander"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="santander"
                                        />{" "}
                                        13
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val7santander"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="santander"
                                        />{" "}
                                        15
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val8santander"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="santander"
                                        />{" "}
                                        18
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val9santander"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="santander"
                                        />{" "}
                                        20
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val10santander"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="santander"
                                        />{" "}
                                        24
                                      </label>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            {/* columna 3 bancos */}
                            <div className="form-group row">
                              <label className="col-form-label col-md-3 col-sm-3 label-align"></label>

                              <div className="col-md-2 col-sm-2">
                                <Checkbox
                                  checkboxClass="icheckbox_flat-green"
                                  increaseArea="20%"
                                  label=" SCOTIABANK"
                                  id="scotiabank"
                                  class="flat"
                                  onChange={this.onChangeCheckboxBanck}
                                  disabled={FormaDePago == "" ? true : false}
                                />
                                {/* este es el menu despegable de meses */}
                                <div
                                  id="Mesesscotiabank"
                                  style={{ display: "none" }}
                                >
                                  <div className="col-md-12">
                                    <p className="mb-1">MSI</p>
                                    <div className="ln_solid mt-0 mb-1 mr-4"></div>
                                  </div>
                                  <div className="col-md-5 col-sm-3">
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val1scotiabank"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="scotiabank"
                                        />{" "}
                                        1
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val2scotiabank"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="scotiabank"
                                        />{" "}
                                        3
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val3scotiabank"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="scotiabank"
                                        />{" "}
                                        6
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val4scotiabank"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="scotiabank"
                                        />{" "}
                                        9
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val5scotiabank"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="scotiabank"
                                        />{" "}
                                        12
                                      </label>
                                    </div>
                                  </div>

                                  <div className="col-md-5 col-sm-5">
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val6scotiabank"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="scotiabank"
                                        />{" "}
                                        13
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val7scotiabank"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="scotiabank"
                                        />{" "}
                                        15
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val8scotiabank"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="scotiabank"
                                        />{" "}
                                        18
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val9scotiabank"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="scotiabank"
                                        />{" "}
                                        20
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val10scotiabank"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="scotiabank"
                                        />{" "}
                                        24
                                      </label>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div className="col-md-2 col-sm-2">
                                <Checkbox
                                  checkboxClass="icheckbox_flat-green"
                                  increaseArea="20%"
                                  label=" SOFOMER"
                                  id="sofomer"
                                  class="flat"
                                  onChange={this.onChangeCheckboxBanck}
                                  disabled={FormaDePago == "" ? true : false}
                                />

                                {/* este es el menu despegable de meses */}
                                <div
                                  id="Mesessofomer"
                                  style={{ display: "none" }}
                                >
                                  <div className="col-md-12">
                                    <p className="mb-1">MSI</p>
                                    <div className="ln_solid mt-0 mb-1 mr-4"></div>
                                  </div>
                                  <div className="col-md-5 col-sm-3">
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val1sofomer"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="sofomer"
                                        />{" "}
                                        1
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val2sofomer"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="sofomer"
                                        />{" "}
                                        3
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val3sofomer"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="sofomer"
                                        />{" "}
                                        6
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val4sofomer"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="sofomer"
                                        />{" "}
                                        9
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val5sofomer"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="sofomer"
                                        />{" "}
                                        12
                                      </label>
                                    </div>
                                  </div>

                                  <div className="col-md-5 col-sm-5">
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val6sofomer"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="sofomer"
                                        />{" "}
                                        13
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val7sofomer"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="sofomer"
                                        />{" "}
                                        15
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val8sofomer"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="sofomer"
                                        />{" "}
                                        18
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val9sofomer"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="sofomer"
                                        />{" "}
                                        20
                                      </label>
                                    </div>
                                    <div className="checkbox icheckbox-green">
                                      <label>
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="val10sofomer"
                                          onClick={
                                            this.onChangeCheckboxBanckMeses
                                          }
                                          name="sofomer"
                                        />{" "}
                                        24
                                      </label>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="form-group row">
                              <label
                                className="col-form-label col-md-3 col-sm-3 label-align"
                                for="last-name"
                              ></label>
                              <div className="col-md-8 col-sm-6">
                                {Bancos.length === 0 ? (
                                  <p className="text-danger mt-0 mb-0 transition">
                                    *Debes seleccionar almenos un banco
                                    participante
                                  </p>
                                ) : null}
                              </div>
                            </div>
                            <Formik
                              initialValues={{ CAT: "", Lineamientos: "" }}
                              validationSchema={SchemaCrearPromo_3}
                              onSubmit={(values, { setSubmitting }) => {
                                setTimeout(() => {
                                  console.log(values);
                                  alert(JSON.stringify(values, null, 2));
                                  setSubmitting(false);
                                }, 400);
                              }}
                            >
                              {({
                                values,
                                errors,
                                touched,
                                handleChange,
                                handleBlur,
                                handleSubmit,
                                isSubmitting,
                                isValid
                                /* and other goodies */
                              }) => (
                                <form onSubmit={this.onSubmit}>
                                  <div className="form-group row">
                                    <label
                                      className="col-form-label col-md-3 col-sm-3 label-align"
                                      for="last-name"
                                    >
                                      CAT <span className="required">*</span>
                                    </label>
                                    <div className="col-md-8 col-sm-6">
                                      <input
                                        name="CAT"
                                        className={
                                          errors.CAT && touched.CAT
                                            ? "form-control is-invalid transition"
                                            : "form-control"
                                        }
                                        onChange={event => {
                                          this.ObtenerInfoInput(event);
                                          handleChange(event);
                                        }}
                                        onBlur={handleBlur}
                                        value={values.CAT}
                                        required
                                      />
                                      {errors.CAT && touched.CAT && (
                                        <p className="text-danger mt-0 mb-0 transition">
                                          {errors.CAT}
                                        </p>
                                      )}
                                    </div>
                                  </div>

                                  <div className="form-group row">
                                    <label
                                      className="col-form-label col-md-3 col-sm-3 label-align"
                                      for="last-name"
                                    >
                                      Lineamientos{" "}
                                      <span className="required">*</span>
                                    </label>
                                    <div className="col-md-8 col-sm-6">
                                      <textarea
                                        name="Lineamientos"
                                        className={
                                          errors.Lineamientos &&
                                          touched.Lineamientos
                                            ? "form-control is-invalid transition"
                                            : "form-control"
                                        }
                                        onChange={event => {
                                          this.ObtenerInfoInput(event);
                                          handleChange(event);
                                        }}
                                        onBlur={handleBlur}
                                        value={values.Lineamientos}
                                        required
                                      ></textarea>
                                      {errors.Lineamientos &&
                                        touched.Lineamientos && (
                                          <p className="text-danger mt-0 mb-0 transition">
                                            {errors.Lineamientos}
                                          </p>
                                        )}
                                    </div>
                                  </div>
                                </form>
                              )}
                            </Formik>

                            <div className="ln_solid"></div>

                            {/* Lista blanca y lista negra */}
                            <div className="row mt-5">
                              <ListaBlanca />
                              <ListaNegra />
                            </div>
                            {/* Lista blanca y lista negra */}
                          </form>
                        </div>

                        <div id="step-4">
                          <div className="d-flex justify-content-center">
                            <div
                              className="x_panel col-md-11 mt-3"
                              style={{ boxShadow: "1px 4px 23px -13px black" }}
                            >
                              <h2 className="mb-4 mt-4">
                                Detalles de la promoción
                              </h2>

                              <div className="table-responsive">
                                <table className="table table-striped ">
                                  <thead>
                                    <tr className="headings">
                                      <th className="column-title">Id </th>
                                      <th className="column-title">
                                        Nombre de la promoción{" "}
                                      </th>
                                      <th className="column-title">
                                        Vigencia{" "}
                                      </th>
                                      <th className="column-title">Origen</th>
                                      <th className="column-title">Estatus</th>
                                      <th className="column-title">
                                        Forma de pago
                                      </th>
                                      <th className="column-title">
                                        Descuento
                                      </th>
                                      <th className="column-title">Monto</th>
                                      <th className="column-title">
                                        Bancos participantes
                                      </th>
                                    </tr>
                                  </thead>

                                  <tbody>
                                    <tr className="even pointer">
                                      <td className=" ">700</td>
                                      <td className=" ">
                                        {NombrePromocion == "" ? (
                                          <p className="text-danger mt-0 mb-0 transition">
                                            *requerido
                                          </p>
                                        ) : (
                                          NombrePromocion
                                        )}
                                      </td>
                                      <td className=" ">
                                        {fechaEscogida == "" ? (
                                          <p className="text-danger mt-0 mb-0 transition">
                                            *requerido
                                          </p>
                                        ) : (
                                          fechaEscogida
                                        )}{" "}
                                      </td>
                                      <td className=" ">
                                        {Origen == "" ? (
                                          <p className="text-danger mt-0 mb-0 transition">
                                            *requerido
                                          </p>
                                        ) : (
                                          Origen
                                        )}
                                      </td>
                                      <td className=" ">
                                        <input
                                          type="checkbox"
                                          className="flat"
                                          id="check2"
                                          disabled
                                          hidden
                                        />
                                        {Estatus ? (
                                          <button className="btn btn-round btn-outline-success">
                                            activa
                                          </button>
                                        ) : (
                                          <button className="btn btn-round btn-outline-danger">
                                            Inactiva
                                          </button>
                                        )}
                                      </td>
                                      <td className=" ">
                                        {FormaDePago == "" ? (
                                          <p className="text-danger mt-0 mb-0 transition">
                                            *requerido
                                          </p>
                                        ) : (
                                          FormaDePago
                                        )}
                                      </td>
                                      <td className=" ">
                                        {TipoDescuento == "Pesos (MX)"
                                          ? "$"
                                          : null}
                                        {Descuento == "" ? (
                                          <p className="text-danger mt-0 mb-0 transition">
                                            *requerido
                                          </p>
                                        ) : (
                                          Descuento
                                        )}
                                        {TipoDescuento == "Porcentaje %"
                                          ? "%"
                                          : null}
                                      </td>
                                      <td className=" last">
                                        {montoMin == "" ? (
                                          <p className="text-danger mt-0 mb-0 transition">
                                            *Monto minimo requerido
                                          </p>
                                        ) : (
                                          "Min -" + montoMin
                                        )}
                                        ,
                                        {montoMax == "" ? (
                                          <p className="text-danger mt-0 mb-0 transition">
                                            *Monto maximo requerido
                                          </p>
                                        ) : (
                                          "Max -" + montoMax
                                        )}
                                      </td>
                                      <td>
                                        {Bancos.length == 0 ? (
                                          <p className="text-danger mt-0 mb-0 transition">
                                            No has selecionado bancos
                                          </p>
                                        ) : (
                                          SoloMsi.map(datos => {
                                            const Msi2 = Object.values(
                                              datos.msi
                                            );
                                            return (
                                              <div>
                                                {datos.Banco}
                                                <br></br>
                                                
                                                {Msi2.map(datos2 => {
                                                  return <span>{datos2.mes},</span>;
                                                })}
                                                <span>meses</span>
                                                <div className="ln_solid"></div>
                                              </div>
                                            );
                                          })
                                        )}
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>

                          <div className="col-md-12 col-md-6 text-center mt-4 mb-4">
                            <button
                              onClick={this.enviarDatos}
                              className="btn btn-success"
                            >
                              Crear promocion
                            </button>
                            <button
                              id="modalDatosExitosos"
                              hidden
                              data-toggle="modal"
                              data-target=".bs-example-modal-sm"
                            >
                              Boton Datos exitosos
                            </button>
                            <button
                              id="modalLLenarDatos"
                              hidden
                              data-toggle="modal"
                              data-target=".modal-LLenarDatos"
                            >
                              Boton Datos exitosos
                            </button>
                          </div>
                        </div>
                      </div>
                      {/* End SmartWizard Content */}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* modal */}
        <PromocionCreada />
        <LLenarDatos clase="modal fade modal-LLenarDatos" />
        {/* /modal */}
      </Fragment>
    );
  }
}

// const mapStateToProps = state => ({
//   datosCategorias: state.data.categorias
// });

// const mapActionToProps = {
//   getCategorias
// };

// export default connect(mapStateToProps, mapActionToProps)(CrearPromocion);

export default CrearPromocion;
