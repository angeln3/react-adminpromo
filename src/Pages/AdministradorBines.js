import React, { Component } from "react";
//Switchery
import Switch from "react-switchery-component";
import { Link } from "react-router-dom";

export class AdministradorBines extends Component {
  constructor(props) {
    super(props);
    this.state = { isChecked: false };
  }

  handleChange = e => {
    const { name, checked } = e.target;
    console.log(name, checked);
    this.setState({ isChecked: checked });
  };

  render() {
    const { isChecked } = this.state;
    return (
      <div className="right_col" role="main">
        <div className="">
          <div className="page-title">
            <div className="title_left">
              <h3>Administrador de Bines </h3>
            </div>
          </div>

          <div className="clearfix"></div>

          <nav aria-label="breadcrumb">
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <a href="#">Home</a>
              </li>
              <li className="breadcrumb-item active">
                <a href="#">Administrador de Bancos</a>
              </li>
              <li className="breadcrumb-item active">
                <a href="#">Buen Fin</a>
              </li>
            </ol>
          </nav>

          <div className="row">
            <div className="col-md-12 col-sm-12  ">
              <div
                className="x_panel"
                style={{ boxShadow: "1px 4px 23px -13px black" }}
              >
                <div className="x_title">
                  <h2>Promoción Buen Fin</h2>

                  <div className="clearfix"></div>
                </div>

                <div className="x_content">
                  <div
                    className="x_panel"
                    style={{ boxShadow: "1px 4px 23px -13px black" }}
                  >
                    <h2 className="col-md-12 breadcrumb">
                      Lista de bancos seleccionados
                    </h2>

                    <div className="clearfix"></div>

                    <div className="table-responsive">
                      <table className="table table-striped ">
                        <thead>
                          <tr className="headings">
                            <th className="column-title">ID </th>
                            <th className="column-title">Banco </th>
                            <th className="column-title">Estado</th>

                            <th className="column-title"></th>
                          </tr>
                        </thead>

                        <tbody>
                          <tr className="even pointer">
                            <td className=" ">400</td>
                            <td className=" ">Afirme</td>
                            <td className=" ">
                              <Switch
                                color="#26B99A"
                                name="400"
                                checked={isChecked}
                                onChange={e => this.handleChange(e)}
                              />
                            </td>

                            <td className=" ">
                              <div className="btn-group">
                                <button
                                  type="button"
                                  className="btn btn-primary dropdown-toggle"
                                  data-toggle="dropdown"
                                  aria-haspopup="true"
                                  aria-expanded="false"
                                >
                                  Acciones
                                </button>
                                <div className="dropdown-menu">
                                  <Link
                                    className="dropdown-item"
                                    to="/VerBines"
                                  >
                                    <i className="fa fa-eye"></i>&nbsp; Ver
                                    bines
                                  </Link>
                                </div>
                              </div>
                            </td>
                          </tr>

                          <tr className="even pointer">
                            <td className=" ">401</td>
                            <td className=" ">Banamex</td>
                            <td className=" ">
                              <Switch
                                color="#26B99A"
                                name="401"
                                checked={isChecked}
                                onChange={e => this.handleChange(e)}
                              />
                            </td>

                            <td className=" ">
                              <div className="btn-group">
                                <button
                                  type="button"
                                  className="btn btn-primary dropdown-toggle"
                                  data-toggle="dropdown"
                                  aria-haspopup="true"
                                  aria-expanded="false"
                                >
                                  Acciones
                                </button>
                                <div className="dropdown-menu">
                                  <Link
                                    className="dropdown-item"
                                    to="/VerBines"
                                  >
                                    <i className="fa fa-eye"></i>&nbsp; Ver
                                    bines
                                  </Link>
                                </div>
                              </div>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AdministradorBines;
