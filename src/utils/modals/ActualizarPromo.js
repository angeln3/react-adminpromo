import React from "react";
//link
import { Link } from "react-router-dom";

export default function ActualizarPromo({ clase }) {
  return (
    <div className={clase} tabindex="-1" role="dialog" aria-hidden="true">
      <div className="modal-dialog modal-sm">
        <div className="modal-content">
          <div className="modal-header">
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div className="modal-body text-center">
            <i
              className="fa fa-check-circle text-center mb-4 mt-3"
              style={{ color: "#16D60D", fontSize: "3.375rem" }}
            ></i>

            <h4 className="mb-4">¡Los datos han sido actualizados!</h4>
          </div>

          <div className="text-center modal-footer">
            <Link
              className="btn btn-secondary mr-auto ml-auto"
              to="/AdministardorDePromociones"
            >
              Continuar
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}
