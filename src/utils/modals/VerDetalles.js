import React from "react";
import { Link } from "react-router-dom";
import $ from "jquery";

export default function VerDetalles() {
  function cerrarModal() {
    $("#cerrarModal").click();
  }

  return (
    <div
      className="modal fade bs-example-modal-lg"
      tabindex="-1"
      role="dialog"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-lg">
        <div className="modal-content">
          <div className="modal-header">
            <h2>Ver Detalles</h2>

            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div className="modal-body d-flex justify-content-center">
            <div
              className="x_panel col-md-11 mt-3"
              style={{ boxShadow: "1px 4px 23px -13px black" }}
            >
              <h2 className="mb-4 mt-4">Detalles de la promoción</h2>
              <div className="ln_solid"></div>

              <div className="mb-2 mt-2 col-md-12 col-sm-6 breadcrumb d-flex justify-content-between">
                <p className="mb-0">ID:</p>

                <p className="mb-0">700</p>
              </div>

              <div className="mb-2 mt-2 col-md-12 col-sm-6  d-flex justify-content-between">
                <p className="mb-0">Nombre de la promoción:</p>

                <p className="mb-0">Buen Fin</p>
              </div>

              <div className="mb-2 mt-2 col-md-12 col-sm-6 breadcrumb d-flex justify-content-between">
                <p className="mb-0">Vigencia:</p>

                <p className="mb-0">01/01/2020 - 01/25/2020</p>
              </div>

              <div className="mb-2 mt-2 col-md-12 col-sm-6  d-flex justify-content-between">
                <p className="mb-0">Origen:</p>

                <p className="mb-0">Claroshop.com</p>
              </div>

              <div className="mb-2 mt-2 col-md-12 col-sm-6 breadcrumb d-flex justify-content-between">
                <p className="mb-0">Estatus:</p>

                <p className="mb-0">
                  <b>Activo</b>
                </p>
              </div>

              <div className="mb-2 mt-2 col-md-12 col-sm-6  d-flex justify-content-between">
                <p className="mb-0">Forma de pago:</p>

                <p className="mb-0">Paypal</p>
              </div>

              <div className="mb-2 mt-2 col-md-12 col-sm-6 breadcrumb d-flex justify-content-between">
                <p className="mb-0">Descuento:</p>

                <p className="mb-0">$500</p>
              </div>

              <div className="mb-2 mt-2 col-md-12 col-sm-6  d-flex justify-content-between">
                <p className="mb-0">Monto:</p>

                <p className="mb-0">Min - 100, Max - 500</p>
              </div>
            </div>
          </div>

          <div className="text-center modal-footer">
            <Link
              onClick={cerrarModal}
              type="button"
              className="btn btn-success mr-auto ml-auto"
              to="/Editar"
            >
              Editar
            </Link>

            <button
              id="cerrarModal"
              type="button"
              data-dismiss="modal"
              aria-label="Close"
              className="btn btn-secondary mr-auto ml-auto"
            >
              Cancelar
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
