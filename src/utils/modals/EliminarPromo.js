import React from "react";

export default function EliminarPromo() {
  return (
    <div
      className="modal fade modal-eliminar"
      tabindex="-1"
      role="dialog"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-sm">
        <div className="modal-content">
          <div className="modal-header">
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div className="modal-body text-center">
            <i
              className="fa fa-warning text-center mb-4 mt-3"
              style={{color: "#DA262D", fontSize: "3.375rem"}}
            ></i>

            <h2 className="mb-4">¿Seguro que deseas eliminar esta promoción?</h2>
          </div>

          <div className="text-center modal-footer">
            <a
              type="button"
              href="form_buttons.html"
              className="btn btn-danger mr-auto ml-auto"
            >
              Eliminar
            </a>

            <button
              type="button"
              data-dismiss="modal"
              aria-label="Close"
              className="btn btn-secondary mr-auto ml-auto"
            >
              Cancelar
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
