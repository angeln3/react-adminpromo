import React from "react";
//import link
import { Link } from "react-router-dom";
import $ from "jquery";

export default function PromocionCreada() {
  function cerrarModal() {
    $("#cerrarModal2").click();
  }

  return (
    <div
      className="modal fade bs-example-modal-sm"
      tabindex="-1"
      role="dialog"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-sm">
        <div className="modal-content">
          <div className="modal-header">
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
              id="cerrarModal2"
            >
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div className="modal-body text-center">
            <i
              className="fa fa-check-circle text-center mb-4 mt-3"
              style={{ color: "#16D60D", fontSize: "3.375rem" }}
            ></i>

            <h4 className="mb-4">¡Los datos han sido guardados!</h4>
            <h2>Para crear la promoción se requiere validar bines</h2>
          </div>

          <div className="text-center modal-footer">
            <Link
              onClick={cerrarModal}
              to="/AdministardorDeBines"
              type="button"
              className="btn btn-secondary mr-auto ml-auto"
            >
              Validar bines
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}
