import * as yup from "yup";

//let yup = require('yup');

//constantes experciones regulares
const charEspeciales = /^[^·/=¿?*^¨Ç´ç{}+]*$/;
const todosCharEspeciales = /^[A-Za-z\u00f1\u00d1ZÀ-ÿ´ 0-9]*$/;

//Validaciones editar promocion
export const SchemaEditarPromo = yup.object().shape({
  FormaDePago: yup.string().required("Campo requerido"),
  Fecha: yup.string().required("Campo requerido"),
  NombrePromocion: yup.string().required("Campo requerido")
});

//Validaciones editar Tarjeta
export const SchemaEditarTarjeta = yup.object().shape({
  NombreTarjeta: yup.string().required("Campo requerido"),
  TipoTarjeta: yup.string().required("Campo requerido"),
  Bin: yup
    .number("Tiene que ser numérico")
    .positive("Tiene que ser positivo")
    .typeError("Tiene que ser numérico")
    .required("Campo requerido")
});

//Validaciones crear promocion paso 1
export const SchemaCrearPromo_1 = yup.object().shape({
  NombrePromocion: yup
    .string()
    .matches(charEspeciales, "No se permite este caracter")
    .required("Campo requerido")
    .max(80, "solo se permiten 80 caracteres"),
  Origen: yup.string().required("Campo requerido"),
  fecha: yup.string().required("Campo requerido")
});

//Validaciones crear promocion paso 2
export const SchemaCrearPromo_2 = yup.object().shape({
  FormaDePago: yup.string().required("Campo requerido"),
  TipoDescuento: yup.string().required("Campo requerido"),
  Descuento: yup
    .number("Tiene que ser numérico")
    .positive("Tiene que ser positivo")
    .typeError("Tiene que ser numérico")
    .required("Campo requerido"),
  montoMin: yup
    .number("Tiene que ser numérico")
    .positive("Tiene que ser positivo")
    .typeError("Tiene que ser numérico")
    .required("Campo requerido")
    .moreThan(0, "Monto mínimo de 1"),
  montoMax: yup
    .number("Tiene que ser numérico")
    .positive("Tiene que ser positivo")
    .required("Campo requerido")
    .typeError("Tiene que ser numérico")
    .moreThan(0, "Monto mínimo de 1")
});

//Validaciones crear promoción paso 3
export const SchemaCrearPromo_3 = yup.object().shape({
  CAT: yup
    .string()
    .required("Campo requerido")
    .matches(todosCharEspeciales, "No se permite este caracter")
    .max(80, "solo se permiten 80 caracteres"),
  Lineamientos: yup
    .string()
    .required("Campo requerido")
    .matches(todosCharEspeciales, "No se permite este caracter")
    .max(80, "solo se permiten 80 caracteres")
});
