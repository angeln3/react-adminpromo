import React from "react";
//route
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
//redux stuff
import { Provider } from "react-redux";
import store from "./redux/store";
//logo
import logo from "./img.jpg";

import "jquery/dist/jquery.min";
//fontawesome (iconos)
//import '@fortawesome/fontawesome-free/css/all.css';
//import '@fortawesome/fontawesome-free/js/all.js';

// Icheck
import "icheck/skins/all.css";

//Date range picker
import "bootstrap-daterangepicker/daterangepicker.css";

//dataTable
import "datatables.net-bs4";
import "datatables.net-bs4/css/dataTables.bootstrap4.css";

//bootstrap
import "bootstrap/dist/css/bootstrap.min.css";
//bootstrap js
import "bootstrap/dist/js/bootstrap.bundle.min.js";
import "bootstrap/dist/js/bootstrap.min.js";

//switchery
import "react-switchery-component/react-switchery-component.css";

//estilos custom gentealella
import "./scss/custom.scss";

//import './js/custom'
//import 'smartwizard/dist/js/jquery.smartWizard';

//Componentes header, footer y menu
import MenuLateral from "./components/MenuLateral/MenuLateral";
import TopNavegacion from "./components/Header/TopNavegacion";
import Footer from "./components/Footer/Footer";

//Componentes Pagina
import CrearPromocion from "./Pages/CrearPromocion";
import AdministardorPromocion from "./Pages/AdministradorPromocion";
import EditarPromocion from "./Pages/EditarPromocion";
import AdministradorBines from "./Pages/AdministradorBines";
import VerBines from "./Pages/VerBines";
import EditarTergetaBin from "./Pages/EditarTargetaBin";
import Prueba from "./Pages/Prueba";

//sagas
// import { useDispatch } from "react-redux";
// import { getCategorias1 } from "./redux/actions/dataActions";

class App extends React.Component {
  render() {
    return (
      <div className="main_container">
        <Provider store={store}>
          <MenuLateral logo={logo} />

          <TopNavegacion logo={logo} />
          {/* --------- page content --------- */}
          <Router>
            <Switch>
              <Route exact path="/" component={AdministardorPromocion} />
              <Route exact path="/CrearPromocion" component={CrearPromocion} />
              <Route exact path="/Editar" component={EditarPromocion} />
              <Route
                exact
                path="/AdministardorDeBines"
                component={AdministradorBines}
              />
              <Route exact path="/VerBines" component={VerBines} />
              <Route
                exact
                path="/EditarTergetaBin"
                component={EditarTergetaBin}
              />
              <Route exact path="/Prueba" component={Prueba} />
            </Switch>
          </Router>

          <Footer />
        </Provider>
      </div>
    );
  }
}

export default App;
