import React from "react";
import $ from "jquery";

class MenuLateral extends React.Component {
  componentDidMount() {
    $("#btnPromociones").click(function() {
      $(this).addClass("active");
      $("#menuPromociones").slideToggle();
    });
  }

  render() {
    const { logo } = this.props;

    return (
      <div className="col-md-3 left_col">
        <div className="left_col scroll-view">
          <div className="navbar nav_title">
            <a href="index.html" className="site_title">
              <i className="fa fa-paw"></i> <span>Gentelella Alela!</span>
            </a>
          </div>

          <div className="clearfix"></div>

          {/* menu profile quick info */}
          <div className="profile clearfix">
            <div className="profile_pic">
              <img src={logo} alt="..." className="img-circle profile_img" />
            </div>
            <div className="profile_info">
              <span>Welcome,</span>
              <h2>John Doe</h2>
            </div>
          </div>
          {/* /menu profile quick info */}

          <br></br>

          {/* sidebar menu */}
          <div
            id="sidebar-menu"
            className="main_menu_side hidden-print main_menu"
          >
            <div className="menu_section">
              <h3>General</h3>
              <ul className="nav side-menu">
                <li id="btnPromociones">
                  <a>
                    - <i className="fa fa-percent"></i> Promociones{" "}
                    <span className="fa fa-chevron-down"></span>
                  </a>
                  <ul className="nav child_menu" id="menuPromociones">
                    <li>
                      <a href="/">Administrar</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
          {/* /sidebar menu */}

          {/* menu footer buttons */}
          <div className="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
              <span
                className="glyphicon glyphicon-cog"
                aria-hidden="true"
              ></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
              <span
                className="glyphicon glyphicon-fullscreen"
                aria-hidden="true"
              ></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
              <span
                className="glyphicon glyphicon-eye-close"
                aria-hidden="true"
              ></span>
            </a>
            <a
              data-toggle="tooltip"
              data-placement="top"
              title="Logout"
              href="login.html"
            >
              <span
                className="glyphicon glyphicon-off"
                aria-hidden="true"
              ></span>
            </a>
          </div>
          {/* / menu footer buttons */}
        </div>
      </div>
    );
  }
}

export default MenuLateral;
