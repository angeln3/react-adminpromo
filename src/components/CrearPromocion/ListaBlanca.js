import React, { Component } from 'react';
//jquery
import $ from 'jquery';
//jstree
import TreeView from 'react-simple-jstree';

export class ListaBlanca extends Component {

    constructor(props){
		super(props);

		this.state = {

            AplicarEn: "",

            data: {
                core: {
                  data: [
                    {
                        text: 'Celulares y telefonía', 
                        id: 'Celulares y telefonía',
                        children: [
                            {
                                text: 'Celulares',
                                id: 'Celulares'
                            },
                            {
                                text: 'Telefonia',
                                id: 'Telefonia'
                            }
                        ]
                    },
                    {
                        text: 'Electrónica y Tecnología', 
                        id: 'Electrónica y Tecnología', 
                        children: [
                          {
                              text: 'Accesorios',
                              id: 'Accesorios'
                         }
                        ]
                    }
                  ]
                },
                "plugins" : ["checkbox"]
              }
              
		}
    }

     //obtener los nodos seleccionados
     handleChangejsTree(e, data) {
        this.setState({
            categoSelected: data.selected,
        })
        //console.log(data)
    }  

    infoInputs = (e) =>{
        const {name, value} = e.target;

        this.setState({
            [name] : value
        })
    }

    onSubmitLB = (e) =>{
        e.preventDefault();
        if(this.state.AplicarEn === 'Categorias' || this.state.AplicarEn === 'Sellers'){
            var datos = {
                AplicarEn: this.state.AplicarEn,
                categoSelected : this.state.categoSelected
            }
        }
        if(this.state.AplicarEn === 'Tienda'){
            var datos = {
                AplicarEn: this.state.AplicarEn,
                Tienda: this.state.Tienda
            }
        }
        if(this.state.AplicarEn === 'Productos'){
            var datos = {
                AplicarEn: this.state.AplicarEn,
                Producto: this.state.Producto
            }
        }
        console.log('Envuar datos...', datos)
    }

    componentDidMount(){
        //funcion para cambiar el input de lista blanca
		   //Aplicar display none y block
        $('#lista_aplica').change(function(){
				
            var Opcion = $(this).val();
            
            
            if(Opcion == 'Categorias'){
                $('#jstree_demo_div').fadeIn();
                $('#Tienda').css("display", "none");
                $('#Producto').css("display", "none");
                $('#lblAplica').html('<p>Lista:</p>');
                
            }else if(Opcion == 'Tienda'){
                $('#Tienda').fadeIn();
                $('#jstree_demo_div').css("display", "none");
                $('#Producto').css("display", "none");
                $('#lblAplica').html('<p>Tienda:</p>');
                
            }else if(Opcion == 'Productos'){
                $('#Producto').fadeIn();
                $('#Tienda').css("display", "none");
                $('#jstree_demo_div').css("display", "none");
                $('#lblAplica').html('<p>Producto:</p>');

            }else if(Opcion == 'Sellers'){
                $('#jstree_demo_div').fadeIn();
                $('#Tienda').css("display", "none");
                $('#Producto').css("display", "none");
                $('#lblAplica').html('<p>Lista:</p>');
            }
            
        });

         //funcion para cerrar tablas de categoria, productos y tienda
		   $('.cerrarTbl').click(function(e){
            console.log(e.target.id); 
             
             var idBtn = e.target.id;
             
             var idBtnConcat = '#'+idBtn+'';
             var idTbl = '#'+idBtn+'_Tabla' ;
             
             $(idTbl).slideToggle();
             $(idBtnConcat).toggleClass('fa-chevron-down'); 
             
         });
    }

    render() {

        const {data} = this.state;

        return (
                <div className="col-md-6 ">
                    <div className="x_panel" style={{boxShadow: "1px 4px 23px -13px black"}}>
                    <div className="x_title">
                        <h2>Lista blanca</h2>
                        <ul className="nav navbar-right panel_toolbox">
                        <li><a className="collapse-link"><i className="fa fa-chevron-up"></i></a>
                        </li>
                        
                        </ul>
                        <div className="clearfix"></div>
                    </div>
                    <div className="x_content">
                        <br />
                        <form onSubmit={this.onSubmitLB} className="form-label-left input_mask">
                        
                        
                        <div className="form-group row">
                            <label className="col-form-label col-md-3 col-sm-3 ">Aplicar en
                            </label>
                            <div className="col-md-9 col-sm-9 ">      
                                
                            <select onChange={this.infoInputs} className="form-control" id="lista_aplica" name="AplicarEn">
                                <option selected="true" disabled="disabled">Seleccione uno</option>
                                <option value="Categorias">Categorias</option>
                                <option value="Tienda">Tienda </option>
                                <option value="Productos">Productos </option>
                                <option value="Sellers">Sellers </option>
                            </select>
                                
                            </div>
                        </div>
                            
                        <div className="form-group row">

                            <label className="col-form-label col-md-3 col-sm-3 " id="lblAplica"></label>
                            
                                    
                                <div id="jstree_demo_div" style={{ display : "none"}}>
                                    
                                    <TreeView treeData={data} onChange={(e, data) => this.handleChangejsTree(e, data)} />
                                   
                                </div>
                            
                                <div className="col-md-9 col-sm-9 "> 
                                    <select class="form-control" name="Tienda" onChange={this.infoInputs} id="Tienda" style={{ display : "none"}}>
                                        <option selected="true" disabled="disabled">Seleccione</option>
                                        <option >Sears</option>
                                        <option >Sanborns</option>
                                        <option >Pier1</option>
                                        <option >Endomex</option>
                                    </select>

                                    <input type="text" name="Producto" onChange={this.infoInputs} id="Producto"
                                    class="form-control"
                                    style={{ display : "none"}}
                                    >

                                    </input>

                                </div>   
                            
                            </div>
                            
                        
                        
                        <div className="form-group row">
                            <label className="col-form-label col-md-3 col-sm-3 ">
                            </label>
                            <div className="col-md-9 col-sm-9 ">       
                            <button type="submit" className="col-md-6 col-sm-6 btn btn-success">Agregar</button>
                            </div>
                        </div>
                                
                        </form>
                        
                        <div className="x_panel" style={{boxShadow: "1px 4px 23px -13px black"}}>  
                        
                            <div className="col-md-12 ">

                                <h2 className="mt-3 col-md-4">Categorias</h2> 
                                <a className="col-md-7 mt-3"><i id="oculta_tbl_catego2" className="fa fa-chevron-up cerrarTbl"></i></a>   

                            </div>  


                            <div className="table-responsive" id="oculta_tbl_catego2_Tabla"> 
                                <table className="table table-striped ">
                                    <thead>
                                    <tr className="headings">

                                        <th className="column-title">Id </th>
                                        <th className="column-title">Nombre</th>
                                        <th className="column-title label-align">
                                            <a>Eliminar todos &nbsp;<i className="fa fa-trash"></i></a>
                                        </th>

                                    </tr>
                                    </thead>
                                </table>	  
                            </div>
                        </div>  
                        
                        
                        <div className="x_panel" style={{boxShadow: "1px 4px 23px -13px black"}}>    
                            <div className="col-md-12 ">

                                <h2 className="mt-3 col-md-4">Productos</h2> 
                                <a className="col-md-7 mt-3"><i id="oculta_tbl_productos2" className="fa fa-chevron-up cerrarTbl"></i></a>   

                            </div>   


                            <div className="table-responsive" id="oculta_tbl_productos2_Tabla">
                                <table className="table table-striped">
                                    <thead>
                                    <tr className="headings">

                                        <th className="column-title">Id </th>
                                        <th className="column-title">Nombre </th>
                                        <th className="column-title label-align">
                                            <a>Eliminar todos &nbsp;<i className="fa fa-trash"></i></a>
                                        </th>

                                    </tr>
                                    </thead>
                                </table>	  
                            </div>
                            
                        </div>	
                        
                        <div className="x_panel" style={{boxShadow: "1px 4px 23px -13px black"}}>  
                        
                            <div className="col-md-12 ">

                                <h2 className="mt-3 col-md-4">Tiendas</h2> 
                                <a className="col-md-7 mt-3"><i id="oculta_tbl_tiendas2" className="fa fa-chevron-up cerrarTbl"></i></a>   

                            </div> 


                            <div className="table-responsive" id="oculta_tbl_tiendas2_Tabla">
                                <table className="table table-striped ">
                                    <thead>
                                    <tr className="headings">

                                        <th className="column-title">Id </th>
                                        <th className="column-title">Nombre </th>
                                        <th className="column-title label-align">
                                            <a>Eliminar todos &nbsp;<i className="fa fa-trash"></i></a>
                                        </th>

                                    </tr>
                                    </thead>
                                </table>	  
                            </div>
                            
                        </div>	

                        <div className="x_panel" style={{boxShadow: "1px 4px 23px -13px black"}}>  
                        
                            <div className="col-md-12 ">

                                <h2 className="mt-3 col-md-4">Sellers</h2> 
                                <a className="col-md-7 mt-3"><i id="oculta_tbl_tiendas3" className="fa fa-chevron-up cerrarTbl"></i></a>   

                            </div> 


                            <div className="table-responsive" id="oculta_tbl_tiendas3_Tabla">
                                <table className="table table-striped ">
                                    <thead>
                                    <tr className="headings">

                                        <th className="column-title">Id </th>
                                        <th className="column-title">Nombre </th>
                                        <th className="column-title label-align">
                                            <a>Eliminar todos &nbsp;<i className="fa fa-trash"></i></a>
                                        </th>

                                    </tr>
                                    </thead>
                                </table>	  
                            </div>
                            
                        </div>	
                        
                        
                    </div>
                </div>
            </div>
                    
   
        )
    }
}

export default ListaBlanca;
