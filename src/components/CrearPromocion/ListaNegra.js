import React, { Component } from 'react';
//jquery
import $ from 'jquery';
//jstree
import TreeView from 'react-simple-jstree';

//redux stuff
import {connect} from 'react-redux';
//import {getCategorias} from '../../redux/actions/dataActions';

export class ListaNegra extends Component {

    constructor(props){
		super(props);

		this.state = {

            AplicarEn: "",

            data: {
                core: {
                  data: [
                    {
                        text: 'Celulares y telefonía', 
                        id: 'Celulares y telefonía',
                        children: [
                            {
                                text: 'Celulares',
                                id: 'Celulares'
                            },
                            {
                                text: 'Telefonia',
                                id: 'Telefonia'
                            }
                        ]
                    },
                    {
                        text: 'Electrónica y Tecnología', 
                        id: 'Electrónica y Tecnología', 
                        children: [
                          {
                              text: 'Accesorios',
                              id: 'Accesorios'
                         }
                        ]
                    }
                  ]
                },
                "plugins" : ["checkbox"]
              }
              
		}
    }

     //obtener los nodos seleccionados
     handleChangejsTree(e, data) {
        this.setState({
            categoSelected: data.selected,
        })
        console.log(data);
    }  

    infoInputs = (e) =>{
        const {name, value} = e.target;

        this.setState({
            [name] : value
        })
    }

    onSubmitLN = (e) =>{
        e.preventDefault();
        if(this.state.AplicarEn === 'Categorias' || this.state.AplicarEn === 'Sellers' ){
            var datos = {
                AplicarEn: this.state.AplicarEn,
                categoSelected : this.state.categoSelected
            }
        }
        if(this.state.AplicarEn === 'Tienda'){
            var datos = {
                AplicarEn: this.state.AplicarEn,
                TiendaLN : this.state.TiendaLN
            }
        }
        if(this.state.AplicarEn === 'Productos'){
            var datos = {
                AplicarEn: this.state.AplicarEn,
                ProductoLN : this.state.ProductoLN
            }
        }
        console.log('Envuar datos...', datos)
    }

    componentDidMount(){
        $('#lista_aplicaLN').change(function(){
				
            var Opcion = $(this).val();
            
            
            if(Opcion == 'Categorias'){
                $('#jstree_demo_divLN').fadeIn();
                $('#TiendaLN').css("display", "none");
                $('#ProductoLN').css("display", "none");
                $('#lblAplicaLN').html('<p>Lista:</p>');
                
            }else if(Opcion == 'Tienda'){
                $('#TiendaLN').fadeIn();
                $('#jstree_demo_divLN').css("display", "none");
                $('#ProductoLN').css("display", "none");
                $('#lblAplicaLN').html('<p>Tienda:</p>');
                
            }else if(Opcion == 'Productos'){
                $('#ProductoLN').fadeIn();
                $('#TiendaLN').css("display", "none");
                $('#jstree_demo_divLN').css("display", "none");
                $('#lblAplicaLN').html('<p>Producto:</p>');

            }else if(Opcion == 'Sellers'){
                $('#jstree_demo_divLN').fadeIn();
                $('#TiendaLN').css("display", "none");
                $('#ProductoLN').css("display", "none");
                $('#lblAplicaLN').html('<p>Lista:</p>');
            }

            
        });
    }

    render() {

        const {data} = this.state;

       // const {dataCatego} = this.props;


        return (
                <div className="col-md-6 ">
                    <div className="x_panel" style={{boxShadow: "1px 4px 23px -13px black"}}>
                    <div className="x_title">
                        <h2>Lista negra</h2>
                        <ul className="nav navbar-right panel_toolbox">
                        <li><a className="collapse-link"><i className="fa fa-chevron-up"></i></a>
                        </li>
                        
                        </ul>
                        <div className="clearfix"></div>
                    </div>
                    <div className="x_content">
                        <br />
                        <form onSubmit={this.onSubmitLN} className="form-label-left input_mask">
                        
                        
                        <div className="form-group row">
                            <label className="col-form-label col-md-3 col-sm-3 ">Aplicar en
                            </label>
                            <div className="col-md-9 col-sm-9 ">      
                                
                            <select onChange={this.infoInputs} className="form-control" id="lista_aplicaLN" name="AplicarEn">
                                <option selected="true" disabled="disabled">Seleccione uno</option>
                                <option value="Categorias">Categorias</option>
                                <option value="Tienda">Tienda </option>
                                <option value="Productos">Productos </option>
                                <option value="Sellers">Sellers </option>
                            </select>
                                
                            </div>
                        </div>
                            
                        <div className="form-group row">

                            <label className="col-form-label col-md-3 col-sm-3 " id="lblAplicaLN"></label>
                            
                                    
                                <div id="jstree_demo_divLN" style={{ display : "none"}}>
                                    
                                    <TreeView treeData={data} onChange={(e, data) => this.handleChangejsTree(e, data)} />
                                   
                                </div>
                            
                                <div className="col-md-9 col-sm-9 "> 
                                    <select class="form-control" name="TiendaLN" onChange={this.infoInputs} id="TiendaLN" style={{ display : "none"}}>
                                        <option selected="true" disabled="disabled">Seleccione</option>
                                        <option >Sears</option>
                                        <option >Sanborns</option>
                                        <option >Pier1</option>
                                        <option >Endomex</option>
                                    </select>

                                    <input type="text" name="ProductoLN" onChange={this.infoInputs} id="ProductoLN"
                                    class="form-control"
                                    style={{ display : "none"}}
                                    >

                                    </input>

                                </div>   
                            
                            </div>
                            
                        
                        
                        <div className="form-group row">
                            <label className="col-form-label col-md-3 col-sm-3 ">
                            </label>
                            <div className="col-md-9 col-sm-9 ">       
                            <button type="submit" className="col-md-6 col-sm-6 btn btn-success">Agregar</button>
                            </div>
                        </div>
                                
                        </form>
                        
                        <div className="x_panel" style={{boxShadow: "1px 4px 23px -13px black"}}>  
                        
                            <div className="col-md-12 ">

                                <h2 className="mt-3 col-md-4">Categorias</h2> 
                                <a className="col-md-7 mt-3"><i id="oculta_tbl_catego" className="fa fa-chevron-up cerrarTbl"></i></a>   

                            </div>  


                            <div className="table-responsive" id="oculta_tbl_catego_Tabla"> 
                                <table className="table table-striped ">
                                    <thead>
                                    <tr className="headings">

                                        <th className="column-title">Id </th>
                                        <th className="column-title">Nombre</th>
                                        <th className="column-title label-align">
                                            <a>Eliminar todos &nbsp;<i className="fa fa-trash"></i></a>
                                        </th>

                                    </tr>
                                    </thead>
                                </table>	  
                            </div>
                        </div>  
                        
                        
                        <div className="x_panel" style={{boxShadow: "1px 4px 23px -13px black"}}>    
                            <div className="col-md-12 ">

                                <h2 className="mt-3 col-md-4">Productos</h2> 
                                <a className="col-md-7 mt-3"><i id="oculta_tbl_productos" className="fa fa-chevron-up cerrarTbl"></i></a>   

                            </div>   


                            <div className="table-responsive" id="oculta_tbl_productos_Tabla">
                                <table className="table table-striped">
                                    <thead>
                                    <tr className="headings">

                                        <th className="column-title">Id </th>
                                        <th className="column-title">Nombre </th>
                                        <th className="column-title label-align">
                                            <a>Eliminar todos &nbsp;<i className="fa fa-trash"></i></a>
                                        </th>

                                    </tr>
                                    </thead>
                                </table>	  
                            </div>
                            
                        </div>	
                        
                        <div className="x_panel" style={{boxShadow: "1px 4px 23px -13px black"}}>  
                        
                            <div className="col-md-12 ">

                                <h2 className="mt-3 col-md-4">Tiendas</h2> 
                                <a className="col-md-7 mt-3"><i id="oculta_tbl_tiendas" className="fa fa-chevron-up cerrarTbl"></i></a>   

                            </div> 


                            <div className="table-responsive" id="oculta_tbl_tiendas_Tabla">
                                <table className="table table-striped ">
                                    <thead>
                                    <tr className="headings">

                                        <th className="column-title">Id </th>
                                        <th className="column-title">Nombre </th>
                                        <th className="column-title label-align">
                                            <a>Eliminar todos &nbsp;<i className="fa fa-trash"></i></a>
                                        </th>

                                    </tr>
                                    </thead>
                                </table>	  
                            </div>
                            
                        </div>	

                        <div className="x_panel" style={{boxShadow: "1px 4px 23px -13px black"}}>  
                        
                            <div className="col-md-12 ">

                                <h2 className="mt-3 col-md-4">Sellers</h2> 
                                <a className="col-md-7 mt-3"><i id="oculta_tbl_tiendas6" className="fa fa-chevron-up cerrarTbl"></i></a>   

                            </div> 


                            <div className="table-responsive" id="oculta_tbl_tiendas6_Tabla">
                                <table className="table table-striped ">
                                    <thead>
                                    <tr className="headings">

                                        <th className="column-title">Id </th>
                                        <th className="column-title">Nombre </th>
                                        <th className="column-title label-align">
                                            <a>Eliminar todos &nbsp;<i className="fa fa-trash"></i></a>
                                        </th>

                                    </tr>
                                    </thead>
                                </table>	  
                            </div>
                            
                        </div>	
                        
                        
                    </div>
                </div>
            </div>
                    
   
        )
    }
}

// const mapStateToProps = (state) =>({
//     dataCatego: state.data.categorias
// })

// const mapActionToProps = {
//     getCategorias
// }

//export default connect(mapStateToProps,mapActionToProps)(ListaNegra);
export default ListaNegra;